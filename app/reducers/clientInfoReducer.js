import constants from '../constants';

/*
  This method returns the new state of the app based on the previous state and the action provided.

*/
export default function(state, action){
  let newState = Object.assign({}, state);

  switch(action.type){
    case constants.actionTypes.SET_CLIENT_INFO:
      return handleSetClientInfoAction(state, action);
      break;
    case constants.actionTypes.INITIALIZE_CLIENT:
      return handleInitializeClientAction(state, action);
      break;
    case constants.actionTypes.RETRY_INITIALIZATION:
      return handleRetryInitialization(state, action);
      break;
    case constants.actionTypes.MARK_TOKEN_VALIDITY:
      return handleMarkTokenValidity(state, action);
      break;
    default:
      return newState;
  }

  return newState;
}


/*
  This method handles token validity.
  the action will include the following info:

  {
    type,
    payload: {
      isValid,
    },
  }
*/
function handleMarkTokenValidity(state, action){
  let newState = Object.assign({}, state);

  let tokenType = ((action||{}).payload || {}).tokenType;
  let tokenValidity = ((action || {}).payload || {}).isValid;

  newState.accessTokenValidity = tokenValidity;

  if(!tokenValidity){
    newState.status = constants.clientStates.INVALID;
  }

  return newState;
}


/*
  This function handles the set client info Action. It sets the token and channels in the State.clientInfo.
  It then sets the client status to OK.
*/
function handleSetClientInfoAction(state, action){
  let newState = Object.assign({}, state);
  let clientInfo = ((action || {}).payload || {}).clientInfo;
  let token = (clientInfo || {}).token;
  let channels = (clientInfo || {}).channels;

  if(token && channels){
    newState.token = (token || {}).access_token;
    newState.channels = channels;
    newState.status = constants.clientStates.OK;
    newState.accessTokenValidity = true;
  }
  else{
    newState.status = constants.clientStates.NOT_INITIALIZIED;
  }

  return newState;
}


/*
  This function handles the initializeClient Action.
  This means that the app will be searching for the client token.
  The returned value is the new state of the app.
*/
function handleInitializeClientAction(state, action){
  let newState = Object.assign({}, state);

  newState.status = constants.clientStates.INITIALIZING;

  return newState;
}

/*
  This function handles the retryInitialization action.
  It puts the app in the state that should retry the initialization process.
  The returned value is the new state of the app.
*/
function handleRetryInitialization(state, action){
  let newState = Object.assign({}, state);

  newState.status = constants.clientStates.NOT_INITIALIZIED;

  return newState;
}
