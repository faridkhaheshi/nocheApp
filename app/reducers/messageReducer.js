/*
  Here we handle the state changes for the messages state.

*/
import constants from '../constants';

export default function(state, action){
  let newState = Object.assign({}, state);

  switch(action.type){
    case constants.actionTypes.ADD_PHOTO:
      return handleAddPhoto(state, action);
      break;
    case constants.actionTypes.UPDATE_PHOTO:
      return handleUpdatePhoto(state, action);
      break;
    case constants.actionTypes.UPDATE_PHOTO_DOWNLOAD_STATUS:
      return handleUpdatePhotoDownloadStatus(state, action);
      break;
    case constants.actionTypes.UPDATE_MESSAGE:
      return handleUpdateMessageAction(state, action);
      break;
    case constants.actionTypes.ADD_MESSAGE:
      return handleAddMessageAction(state, action);
      break;
    case constants.actionTypes.SEND_MESSAGE:
      return handleSendMessageAction(state, action);
      break;
    case constants.actionTypes.SEND_CALLBACK_DATA:
      return handleSendCallbackAction(state, action);
      break;
    case constants.actionTypes.LOAD_MESSAGES:
      return handleLoadMessagesAction(state, action);
      break;
    case constants.actionTypes.SET_LOADING_STATE:
      return handleSetLoadingStateAction(state, action);
      break;
    case constants.actionTypes.MARK_LOADING_MORE_MESSAGES_STATUS:
      return handleMarkLoadingMoreStatusAction(state, action);
      break;
    case constants.actionTypes.ADD_MORE_MESSAGES:
      return handleAddMoreMessagesAction(state, action);
      break;
    case constants.actionTypes.SELECT_MESSAGE:
      return handleSelectMessageAction(state, action);
      break;
    case constants.actionTypes.CHANGE_SCROLL_TO_BOTTOM_STATE:
      return handleChangeScrollToBottomState(state, action);
      break;
    case constants.actionTypes.START_APP:
      return handleStartAppAction(state, action);
      break;
    default:
      return newState;
  }
}



function handleStartAppAction(state, action){
  let newState = Object.assign({}, state);

  newState.loadingState = constants.messageLoadingStates.fresh;

  return newState;
}


function handleChangeScrollToBottomState(state, action){
  let newState = Object.assign({}, state);
  let newValue = action.payload.changeTo;

  newState.shouldScrollToBottom = newValue;

  return newState;
}


/*
  The action payload will include the key to the message:
  {
    type,
    payload:{
      messageKey,
    },
  }
*/
function handleSelectMessageAction(state, action){
  let newState = Object.assign({}, state);

  if(action.payload.messageKey){
    newState.selectedMessageKey = action.payload.messageKey;
  }
  else{
    delete newState.selectedMessageKey;
  }

  return newState;
}


/*
  This method handles the UPDATE_PHOTO_DOWNLOAD_STATUS actions.
  the action payload will include the photoKey and the new status.

  the action:
  {
    type,
    payload:{
      photoKey,
      status,
    },
    photoMap,
  }
*/
function handleUpdatePhotoDownloadStatus(state, action){
  let photoKey = action.payload.photoKey;
  let photoMap = action.payload.photoMap;

  if(photoKey){
    let thePhoto = photoMap[photoKey];
    if(thePhoto){
      thePhoto.download_status = action.payload.status;
    }
  }

  return state;
}

/*
  This method handles the addphoto action type.
  It adds the photo to the photoMap using the provided key.
  If the photo includes a uri field, the key will be equal to the uri.
  If not, the key will be equal to the local_uri field.
  The photo will be provided through action.payload.photo.
  The action will include the following data:
  {
    type,
    payload:{
      photo,
      photoMap,
    },
  }
*/
function handleAddPhoto(state, action){
  //let newState = Object.assign({}, state);
  let photo = action.payload.photo;
  let photoMap = action.payload.photoMap;

  if(photo.uri){
    photoMap[photo.uri] = photo;
    photoMap[photo.uri].download_status = constants.downloadImageStates.WAITING;
  }
  else if(photo.local_uri){
    photoMap[photo.local_uri] = photo;
    photoMap[photo.local_uri].upload_status = constants.uploadImageStates.WAITING;
  }

  return state;
}

/*
  This method handles the Update Photo action type.
  The photo will be available in state.photoMap[photoKey]
  where:
  photoKey is available in action.payload.photoKey.
  The new photo object will be available in action.payload.photo.

  If the new photo has a new photoKey, the photo will be saved to the new key
  as well.
  the action will include the following info:
  {
    type,
    payload:{
      photo,
      photoKey,
      photoMap,
    },
  }

*/
function handleUpdatePhoto(state, action){

  let newPhoto = action.payload.photo;
  let oldPhotoKey = action.payload.photoKey;
  let photoMap = action.payload.photoMap;
  let updatedPhoto = photoMap[oldPhotoKey];
  let newPhotoKey = newPhoto.uri || newPhoto.local_uri;
  let keyHasChanged = (oldPhotoKey !== newPhotoKey);

  for(let key in newPhoto){
    updatedPhoto[key] = newPhoto[key];
  }

  if(action.error){
    if(oldPhotoKey){
      photoMap[oldPhotoKey] = updatedPhoto;
      photoMap[oldPhotoKey].error = action.error;
    }
  }
  else if(keyHasChanged){
    photoMap[newPhotoKey] = updatedPhoto;
  }

  return state;
}

/*
  This method updates a given message.
  The message is in the action.payload.message and the draftIndex (if any) is in the
  action.payload.draftIndex.

  The action will include the following info:

  {
    type,
    payload:{
      message,
      draftIndex,
      messageList,
      messageMap,
    },
  }
*/
function handleUpdateMessageAction(state, action){
  let newState = Object.assign({}, state);

  let draftIndex = action.payload.draftIndex;
  let theMessage = action.payload.message;
  let messageList = newState.messageList;
  let messageMap = action.payload.messageMap;

  if(draftIndex >= 0){
    let draftKey = `d${draftIndex}`;
    if(!isNaN(theMessage.message_index)){
      let index = messageList.indexOf(draftKey);

      if(index > -1){
        messageList[index] = theMessage.message_index;
        messageMap[theMessage.message_index] = theMessage;
        delete messageMap[draftKey];
      }
    }
    else{
      messageMap[draftKey] = theMessage;
    }
  }

  return state;
}

/*
  This method adds a new message to the draftMap and messageList.
  It uses the draftIndex provided through action.payload.draftIndex to find the
  index used for the last draft.
  the new message is available in action.payload.message.
  the action will have the following info:

  {
    type,
    payload:{
      message,
      draftIndex,
      messageList,
      messageMap,
    },
  }
*/
function handleAddMessageAction(state, action){
  let newState = Object.assign({}, state);
  let messageList = newState.messageList;
  let newMessage = action.payload.message;
  let messageMap = action.payload.messageMap;
  let draftIndex = action.payload.draftIndex;
  let draftKey = `d${draftIndex}`;

  increaseDraftIndex(newState);

  messageMap[draftKey] = newMessage;
  messageList.unshift(draftKey);

  return newState;
}


/*
  This method changes the loadingMoreStatus in the appState.message.
  the action will include the new state as payload.
*/
function handleMarkLoadingMoreStatusAction(state, action){
  let newState = Object.assign({}, state);

  newState.loadingMoreStatus = action.payload.status;

  return newState;
}

/*
  This method adds the new message to the appState.messages.
  the action.payload will include:
  {
    messages: [],
    options: { pointer, dir, total },
    messageMap,
  }
*/
function handleAddMoreMessagesAction(state, action){

  let newState = Object.assign({}, state);
  let options = action.payload.options;
  let newMessages = action.payload.messages;
  let oldMessageList = newState.messageList;
  let messageMap = action.payload.messageMap;
  let newMessagesList = [];
  let fullMessageList = [];

  newMessages.sort((a, b) => {return b.message_index-a.message_index;});

  newMessages.forEach((message) => {
    let messageIndex = message.message_index;
    newMessagesList.push(messageIndex);
    messageMap[messageIndex] = message;
  });


  if(options.dir === 'down'){
    // newMessages should be added to the end of old messages.
    fullMessageList = oldMessageList.concat(newMessagesList);
  }
  else if(options.dir === 'up'){
    // newMessages should be added before oldMessages.
    fullMessageList = newMessagesList.concat(oldMessageList);
  }

  let fullMessageLen = fullMessageList.length;
  newState.endPointer = fullMessageList[0];
  newState.startPointer = fullMessageList[fullMessageLen-1];

  newState.messageList = fullMessageList;
  if(newState.startPointer === 1){
    newState.loadingMoreStatus = constants.loadingMoreMessagesStates.finished;
  }
  else{
    newState.loadingMoreStatus = constants.loadingMoreMessagesStates.no;
  }
  return newState;
}


/*
  This method handles loading of the messages.
  The action will include the messages.
  {
    messages: [],
    messageMap,
  }
*/
function handleLoadMessagesAction(state, action){
  const newState = Object.assign({}, state);
  const messages = action.payload.messages;
  const messageMap = action.payload.messageMap;
  newState.messageList = [];
  const messageList = newState.messageList;

  messages.sort((a, b) => {return b.message_index-a.message_index;});
  messages.forEach((message) => {
    let messageIndex = message.message_index;
    messageList.push(messageIndex);
    messageMap[messageIndex] = message;
  });

  let messageListLen = messageList.length;

  if(messageListLen > 0){
    newState.endPointer = messageList[0];
    newState.startPointer = messageList[messageListLen-1];
  }

  newState.loadingState = constants.messageLoadingStates.loaded;
  newState.waitingForMoreStatus = constants.loadingMoreMessagesStates.no;

  //action.payload.messageList.push(2);

  return newState;
}


function handleSetLoadingStateAction(state, action){
  let newState = Object.assign({}, state);

  newState.loadingState = action.payload.state;

  return newState;
}

/*
  This method handles the sendCallbackData action.
  The action have the following data:
  {
    type: 'SEND_CALLBACK_DATA',
    payload: {
      status: 'WAITING' | 'ACTIVE' | 'FAILED'
      index: 0,
      messageIndex: 212,
      text: 'theText',
      callback_data: 'some data'
    }
  }

  we should find the inlineKey using the messageIndex and index and update it.
*/
function handleSendCallbackAction(state, action){
  let newStateObj = Object.assign({}, state);
  let newStateArray = Object.keys(newStateObj).map(key => newStateObj[key]);

  if(action.payload.messageIndex){
    if(action.payload.index){
      let theMessageIndex = newStateArray.findIndex((message) => {
        return(message.message_index === action.payload.message_index);
      });

      let theMessage = newStateArray[theMessageIndex];
      if(Array.isArray(theMessage.inline_keyboard)){
        if(theMessage.inline_keyboard.length > action.payload.index){
          newStateArray[theMessageIndex].inline_keyboard[action.payload.index].status = action.payload.status;
        }
      }
    }
  }

  return Object.assign({}, newStateArray);
}

/*
  This method increments the lastDraftState in the state.
  it changes the input state
*/
function increaseDraftIndex(state){
  let oldDraftIndex = state.nextDraftIndex || 0;
  if(isNaN(oldDraftIndex)){
    state.nextDraftIndex = 0;
  }
  else{
    state.nextDraftIndex = oldDraftIndex + 1;
  }
}
