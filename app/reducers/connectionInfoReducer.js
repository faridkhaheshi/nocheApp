import constants from '../constants';

/*
  This method returns the new state of the app based on the previous state and the action provided.

*/
export default function(state, action){
  let newState = Object.assign({}, state);

  switch(action.type){
    case constants.actionTypes.MARK_SUBSCRIPTION_STATE:
      return handleMarkSubscriptionStateAction(state, action);
      break;
    case constants.actionTypes.MARK_CONNECTION_STATE:
      return handleMarkConnectionStateAction(state, action);
      break;
    case constants.actionTypes.SET_CLIENT_INFO:
      return handleSetClientInfoAction(state, action);
      break;
    default:
      return newState;
  }
}



function handleSetClientInfoAction(state, action){
  let newState = Object.assign({}, state);

  newState.subscriptionStatus = constants.subscriptionStates.NOT_SUBSCRIBED;

  return newState;
}

/*
  This method handles the MARK_SUBSCRIPTION_STATE action.
  Here, the action payload will include the followings:
  {
    serverConnection,
    state,
    channel,
    channels
  }

  if the subscription is done and the action payload indicates that
  the new state is SUBSCRIBED, then we add the name of the channel to the appropriate
  field in the state.

  if all channels are subscribed to, then we change the subscription state to
  SUBSCRIBED.
  if the action indicates NOT_SUBSCRIBED, then we will delete the channel from state
  (if it exists).

*/
function handleMarkSubscriptionStateAction(state, action){

  let newState = Object.assign({}, state);
  let channelName = action.payload.channel || '';
  let subscriptionState = action.payload.state;
  let channels = action.payload.channels;
  let channelInfo = channelName.split('-');
  let channelType = false;

  if(subscriptionState === constants.subscriptionStates.SUBSCRIBED){
    let allChannelsSubscribed = false;

    if(channelInfo.length > 2){
      channelType = channelInfo[1];
      if(channelType === 'device'){
        newState.deviceChannelName = channelName;
        newState.primaryChannelName = channelName;
      }
      else if(channelType === 'user'){
        newState.userChannelName = channelName;
      }
    }

    if(channels.length === 2){
      if(newState.deviceChannelName && newState.userChannelName){
        allChannelsSubscribed = true;
      }
    }
    else if(channels.length === 1){
      if(newState.deviceChannelName){
        allChannelsSubscribed = true;
        newState.primaryChannelName = channelName;
      }
    }

    if(allChannelsSubscribed){
      newState.subscriptionStatus = constants.subscriptionStates.SUBSCRIBED;
    }
  }
  else if(subscriptionState === constants.subscriptionStates.NOT_SUBSCRIBED){
    if(channelInfo.length > 2){
      channelType = channelInfo[1];
      if(channelType === 'device'){
        delete newState.deviceChannelName;
        delete newState.primaryChannelName;
      }
      else if(channelType === 'user'){
        delete newState.userChannelName;
        delete newState.primaryChannelName;
      }

      newState.subscriptionStatus = subscriptionState;

    }
  }
  else{
    newState.subscriptionStatus = subscriptionState;
  }

  return newState;
}



/*
  This method is responsible for reflecting connection status to appState.
  The action payload will include the following fields:
  {
    serverConnection,
    state
  }
*/
function handleMarkConnectionStateAction(state, action){
  let newState = Object.assign({}, state);
  newState.connectionStatus = action.payload.state;

  return newState;
}
