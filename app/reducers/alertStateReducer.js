import constants from '../constants';

/*
  This method returns the new state of the app based on the previous state and the action provided.

*/
export default function(state, action){
  let newState = Object.assign({}, state);

  switch(action.type){
    case constants.actionTypes.SHOW_ALERT:
      return handleShowAlertAction(state, action);
      break;
    case constants.actionTypes.HIDE_ALERT:
      return handleHideAlertAction(state, action);
      break;
    default:
      return newState;
  }
}


function handleShowAlertAction(state, action){
  let newState = Object.assign({}, state);
  let alertText = action.payload.text;
  let colorState = action.payload.colorState;

  newState.shown = true;
  newState.text = alertText;
  newState.colorState = colorState;

  return newState;
}


function handleHideAlertAction(state, action){
  let newState = Object.assign({}, state);

  newState.shown = false;
  delete newState.text;
  delete newState.colorState;

  return newState;
}
