import constants from '../constants';

/*
  This method returns the new state of the app based on the previous state and the action provided.
*/
export default function(state, action){
  let newState = Object.assign({}, state);

  switch(action.type){
    case constants.actionTypes.SET_DEVICE_STATE:
      return handleSetDeviceStateAction(state, action);
      break;
    default:
      return newState;
  }
}


function handleSetDeviceStateAction(state, action){
  let newState = Object.assign({}, state);

  let newDeviceState = action.payload;

  newState.width = newDeviceState.width;
  newState.height = newDeviceState.height;
  newState.isPortrait = newDeviceState.isPortrait;

  return newState;
}
