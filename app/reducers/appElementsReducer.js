import constants from '../constants';

/*
  This method returns the new state of the app based on the previous state and the action provided.
*/
export default function(state, action){
  let newState = Object.assign({}, state);

  switch(action.type){
    case constants.actionTypes.TOGGLE_PHOTO_MODAL_VISIBILITY:
      return handleTogglePhotoModalVisibilityAction(state, action);
      break;
    default:
      return newState;
  }
}


/*
  This method handles the change of state of photo modal visibility.

*/
function handleTogglePhotoModalVisibilityAction(state, action){
  let newState = Object.assign({}, state);

  newState.showPhotoModal = !state.showPhotoModal;

  return newState;
}
