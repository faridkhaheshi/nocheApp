import messageReducer from './messageReducer';
import clientInfoReducer from './clientInfoReducer';
import connectionInfoReducer from './connectionInfoReducer';
import alertStateReducer from './alertStateReducer';
import appElementsReducer from './appElementsReducer';
import deviceStateReducer from './deviceStateReducer';

import { combineReducers } from 'redux';

export default combineReducers({
  messages: messageReducer,
  clientInfo: clientInfoReducer,
  connectionInfo: connectionInfoReducer,
  alertState: alertStateReducer,
  appElements: appElementsReducer,
  deviceState: deviceStateReducer,
});
