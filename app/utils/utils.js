'use strict'
import persianDate from 'moment-jalaali';
import constants from '../constants';
import DeviceInfo from 'react-native-device-info';
import imageResizer from 'react-native-image-resizer';

/*
  This method reduces the size of a given image.
  This is done before uploading a picture.
  This method returns a promise resolving with the new image uri, its width and height.
  The input image will have the following info:
  {
    uri,
    width,
    height,
  }
  The resolved value will have the following info:
  {
    uri,
    width,
    height,
  }
*/
export function reduceImageSize(image){
  return new Promise((resolve, reject) => {
    let newImageSize = resizeImage({width: image.width, height: image.height});
    let newWidth = newImageSize.width;
    let newHeight = newImageSize.height;

    imageResizer.createResizedImage(image.uri, newWidth, newHeight, 'JPEG', constants.imageCompressionParameters.quality)
      .then((resizedImageUri) => {
        resolve({
          uri: resizedImageUri,
          width: newWidth,
          height: newHeight,
        });
      })
      .catch((err) => {
        console.log('imageResizer rejected:');
        console.log(err);
        reject(err);
      });
  });
}

/*
  This method returns the resized image size based on the compression starndard.
  It returns the new dimensions as:
  {
    width: newWidth,
    height: newHeight,
  }
*/
function resizeImage(imageDimensions){
  let oldWidth = imageDimensions.width;
  let oldHeight = imageDimensions.height;
  let newWidth;
  let newHeight;

  if(oldWidth > oldHeight){
    newWidth = constants.imageCompressionParameters.maxDim;
    newHeight = newWidth * (oldHeight/oldWidth);
  }
  else{
    newHeight = constants.imageCompressionParameters.maxDim;
    newWidth = newHeight * (oldWidth/oldHeight);
  }

  return {width: newWidth, height: newHeight};
}


/*
  This method converts the image url to the form appropriate for the specified
  place.
  It returns a URL to be used.
*/
export function convertImageURL(imageUrl, context, DPR){
  // https://res.cloudinary.com/nimvajabicloud/image/upload/v1494072028/xp9y0ze9fyjbdujiglka.jpg

  let baseImageUrl = constants.baseImageCdnURL;
  let dpr = DPR || 'auto';

  //let imageURL = (imageUrl | '');
  let fileName = imageUrl.split('/').pop();

  switch(context){
    case constants.imageContexts.messagePhoto:
      return `${baseImageUrl}${constants.imageQualityParameters.messagePhoto},dpr_${dpr}/${fileName}`;
      break;
    case constants.imageContexts.messageThumbnail:
      return `${baseImageUrl}${constants.imageQualityParameters.messageThumbnail}/${fileName}`;
      break;
    default:
      return imageUrl;
  }
}

/*
  this method takes a timeStamp as an integer and returns the string that must be shown
  to the user.
  The output string in like this:
  ۱۹:۴۵ ۱۱/۱۲
*/
export function translateTimeStamp(timeStamp){
  let dateTimeStr = persianDate(timeStamp).format('jM/jD HH:mm');
  return dateTimeStr.convertNumbersToPersian();
}

export function translateMessageState(messageState){
  switch(messageState){
    case constants.messageStates.INDEXED_ON_SERVER:
      return String.fromCharCode('0x2713');
      break;
    case constants.messageStates.WAITING_TO_BE_INDEXED:
      return '';
      break;
    case constants.messageStates.FAILED_TO_INDEX:
      return String.fromCharCode('0x2717');
      break;
    default:
      return '';
  }
}

/*
  This method returns a promise resolving after specified time in miliseconds.
*/
export function delay(someMiliSeconds){
  return new Promise((resolve, reject) => {
    setTimeout(()=>{
      resolve();
    }, someMiliSeconds);
  });
}



/*
  This method converts numerical digits in an string to persoan:
  '12312 asdasdas 123123' => '۱۲۳۱۲ asdasdas ۱۲۳۱۲۳'
*/

String.prototype.convertNumbersToPersian = function(){
  return this.replace(new RegExp('0','g'),'۰')
              .replace(new RegExp('1','g'),'۱')
              .replace(new RegExp('2','g'),'۲')
              .replace(new RegExp('3','g'),'۳')
              .replace(new RegExp('4','g'),'۴')
              .replace(new RegExp('5','g'),'۵')
              .replace(new RegExp('6','g'),'۶')
              .replace(new RegExp('7','g'),'۷')
              .replace(new RegExp('8','g'),'۸')
              .replace(new RegExp('9','g'),'۹');
}


/*
  This method returns device info as an Object in this form:
  {
    uniqueId: '611ED530-8A7A-47B5-B461-B8F61758ACED',
    manufacturer: 'Apple',
    deviceBrand: 'Apple',
    deviceModel: 'Simulator',
    deviceId: 'x86_64',
    deviceName: 'iOS',
    deviceVersion: '10.2',
    bundleId: 'ir.noche.app.ios',
    buildNumber: 1,
    appVersion: '1.0',
    readableAppVersion: '1.0.1',
    deviceName: 'Mac’s iMac',
    userAgent: 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2 like Mac OS X) AppleWebKit/602.3.12 (KHTML, like Gecko) Mobile/14C89',
    deviceLocale: 'en',
    deviceCountry: 'US',
    appInstanceId: ''           // for android
  }
*/
export function fetchDeviceInfo(){

  return {
    device_unique_id: DeviceInfo.getUniqueID(),
    manufacturer: DeviceInfo.getManufacturer(),
    device_brand: DeviceInfo.getBrand(),
    device_model: DeviceInfo.getModel(),
    device_id: DeviceInfo.getDeviceId(),
    system_name: DeviceInfo.getSystemName(),
    device_version: DeviceInfo.getSystemVersion(),
    bundle_id: DeviceInfo.getBundleId(),
    build_number: DeviceInfo.getBuildNumber(),
    app_version: DeviceInfo.getVersion(),
    readable_app_version: DeviceInfo.getReadableVersion(),
    device_name: DeviceInfo.getDeviceName(),
    user_agent: DeviceInfo.getUserAgent(),
    device_locale: DeviceInfo.getUserAgent(),
    device_country: DeviceInfo.getDeviceCountry(),
    app_instance_id: DeviceInfo.getInstanceID()
  };
}
