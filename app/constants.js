/*
  Here, we have all the states of different things in our model.
*/

const messageStates = {
  BEING_PREPARED: 'BEING_PREPARED',
  WAITING_TO_BE_INDEXED: 'WAITING_TO_BE_INDEXED',
  INDEXED_ON_SERVER: 'INDEXED_ON_SERVER',
  FAILED_TO_INDEX: 'FAILED_TO_INDEX',
};

const messageSenders = {
  BOT: 'BOT',
  USER: 'USER'
};

const uploadImageStates = {
  WAITING: 'WAITING',
  UPLOADING: 'UPLOADING',
  UPLOADED: 'UPLOADED',
  FAILED: 'FAILED',
};

const downloadImageStates = {
  WAITING: 'WAITING',
  DOWNLOADING: 'DOWNLOADING',
  DOWNLOADED: 'DOWNLOADED',
  FAILED: 'FAILED',
};

const serverErrors = {
  NETWORK_PROBLEM: 'NETWORK_PROBLEM',
  UNSUCCESSFUL_RESPONSE: 'UNSUCCESSFUL_RESPONSE',
  TIMEOUT: 'TIMEOUT',
};

const imageContexts = {
  messagePhoto: 'messagePhoto',
  messageThumbnail: 'messageThumbnail',
};

const imageQualityParameters = {
  messagePhoto: 'w_300',
  messageThumbnail: 'w_40,q_10',
};

const actionTypes = {
  ADD_MESSAGE: 'ADD_MESSAGE',
  UPDATE_MESSAGE: 'UPDATE_MESSAGE',
  SEND_MESSAGE: 'SEND_MESSAGE',
  REMOVE_MESSAGE: 'REMOVE_MESSAGE',
  SELECT_MESSAGE: 'SELECT_MESSAGE',
  ADD_PHOTO: 'ADD_PHOTO',
  UPDATE_PHOTO: 'UPDATE_PHOTO',
  LOAD_IMAGE:'LOAD_IMAGE',
  UPDATE_PHOTO_DOWNLOAD_STATUS: 'UPDATE_PHOTO_DOWNLOAD_STATUS',
  CHANGE_SCROLL_TO_BOTTOM_STATE: 'CHANGE_SCROLL_TO_BOTTOM_STATE',


  FETCH_MESSAGES: 'FETCH_MESSAGES',
  LOAD_MESSAGES: 'LOAD_MESSAGES',
  ADD_MORE_MESSAGES: 'ADD_MORE_MESSAGES',
  NOTHING: 'NOTHING',
  MARK_TOKEN_VALIDITY: 'MARK_TOKEN_VALIDITY',
  MARK_DEADEND: 'MARK_DEADEND',


  OPEN_WEB_PAGE: 'OPEN_WEB_PAGE',
  SEND_CALLBACK_DATA: 'SEND_CALLBACK_DATA',
  SET_CLIENT_INFO: 'SET_CLIENT_INFO',
  INITIALIZE_CLIENT: 'INITIALIZE_CLIENT',
  RETRY_INITIALIZATION: 'RETRY_INITIALIZATION',
  MARK_SUBSCRIPTION_STATE: 'MARK_SUBSCRIPTION_STATE',
  MARK_CONNECTION_STATE: 'MARK_CONNECTION_STATE',
  SHOW_ALERT: 'SHOW_ALERT',
  HIDE_ALERT: 'HIDE_ALERT',
  SET_LOADING_STATE: 'SET_LOADING_STATE',
  MARK_LOADING_MORE_MESSAGES_STATUS: 'MARK_LOADING_MORE_MESSAGES_STATUS',
  TOGGLE_PHOTO_MODAL_VISIBILITY: 'TOGGLE_PHOTO_MODAL_VISIBILITY',
  SET_DEVICE_STATE: 'SET_DEVICE_STATE',
};

const buttonStates = {
  WAITING: 'WAITING',
  ACTIVE: 'ACTIVE',
  FAILED: 'FAILED'
};

const viewParams = {
  minWidthForInlineKeys: 150,
  defaultImageWidth: 300,
  imageAspectRatio: 4/3,
};

const clientStates = {
  STARTING: 'STARTING',
  INITIALIZING: 'INITIALIZING',
  NOT_INITIALIZIED: 'NOT_INITIALIZIED',
  OK: 'OK',
  INVALID: 'INVALID',
};

const connectionStates = {
  INITIALIZED: 'INITIALIZED',
  CONNECTING: 'CONNECTING',
  CONNECTED: 'CONNECTED',
  UNAVAILABLE: 'UNAVAILABLE',
  FAILED: 'FAILED',
  DISCONNECTED: 'DISCONNECTED'
};

const subscriptionStates = {
  NOT_SUBSCRIBED: 'NOT_SUBSCRIBED',
  SUBSCRIBED: 'SUBSCRIBED',
  SUBSCRIBING: 'SUBSCRIBING',
  FAILED: 'FAILED',
};

const screenNames = {
  Chat: 'Chat',
  Web: 'Web',
  Welcome: 'Welcome'
};

const appParams = {
  loadTime: 2000,        // ms
  photoPerPage: 40,
  textInputDefaultHeight: 32,
  quickInputLineHeight: 32,
};

const sensitiveInfoOptions = {
  sharedPreferencesName: 'nocheSharedPrefs',
  keychainService: 'nocheKeyChain'
};

const baseServerURL = 'https://nocheserver.herokuapp.com';
const baseImageCdnURL = 'https://res.cloudinary.com/nimvajabicloud/image/upload/';

const pusherAppInfo = {
  appId: '325869',
  appKey: 'df2942106f5876a7cc9b',
  cluster: 'eu',
  isEncrypted: true,
  authEndpoint: 'https://nocheserver.herokuapp.com/pusher/auth'
};

const clientInfoKey = 'clientInfo';
const clientTokenKey = 'noche_client_token';
const alertBoxColorState = {
  red: 'red',
  green: 'green',
  yellow: 'yellow'
};

const alertBoxColorMap = {
  red: 'lightsalmon',
  green: 'powderblue',
  yellow: 'papayawhip'
};

const messageLoadingStates = {
  fresh: 'fresh',
  loading: 'loading',
  loaded: 'loaded',
  failed: 'failed',
  error: 'error',
};

const colors = {
  baseBackground: '#EBF5FB',
};

const maxMessageQueryNum = 40;


const loadingMoreMessagesStates = {
  no: 'no',
  waiting: 'waiting',
  failed: 'failed',
  finished: 'finished',
};

const imageCompressionParameters = {
  maxDim: 1200,
  quality: 50,
};

const texts = {
  textInputPlaceholderText: 'فرمایش‌تون',
};

const tokenTypes = {
  access: 'access',
  refresh: 'refresh',
};

export default {
  messageStates,
  messageSenders,
  actionTypes,
  viewParams,
  buttonStates,
  clientStates,
  screenNames,
  appParams,
  sensitiveInfoOptions,
  clientInfoKey,
  pusherAppInfo,
  baseServerURL,
  clientTokenKey,
  connectionStates,
  subscriptionStates,
  alertBoxColorMap,
  messageLoadingStates,
  colors,
  maxMessageQueryNum,
  loadingMoreMessagesStates,
  uploadImageStates,
  downloadImageStates,
  serverErrors,
  imageContexts,
  baseImageCdnURL,
  imageQualityParameters,
  imageCompressionParameters,
  texts,
  tokenTypes,
};
