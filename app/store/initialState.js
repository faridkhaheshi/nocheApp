import constants from '../constants';

const messages = {
  loadingState: constants.messageLoadingStates.fresh,
  messageList: [
    //message_index || draftKey, where draftKey = `d${draftIndex}`
  ],
  /*
    startPointer
    endPointer
    loadingMoreStatus
    draftCounter
    nextDraftIndex
  */
  selectedMessageKey: null,
  shouldScrollToBottom: false,
};


const clientInfo = {
  status: constants.clientStates.STARTING,
  // token: 'undefined'
  // channels: []
  //accessTokenValidity: true,
  //refreshTokenValidity: true,
  //reachedDeadEnd,
};

const connectionInfo = {
  subscriptionStatus: constants.subscriptionStates.NOT_SUBSCRIBED,
  connectionStatus: constants.connectionStates.DISCONNECTED,
  //userChannelName
  //deviceChannelName
  //primaryChannelName
};

const alertState = {
  shown: false,
  text: 'hi',
  colorState: 'green', // red | green | yellow
};

const deviceState = {
  //width:
  //height:
  isPortrait: true,
};

const appElements = {
  showPhotoModal: false,
};

const initialState = {
  messages,
  clientInfo,
  connectionInfo,
  alertState,
  appElements,
  deviceState
};

export default initialState;
