/*
  Here, we have the overall state of the app.
  We are using the redux framework, thus, this is the only place state can change.
*/

import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import initialState from './initialState';
import allReducers from '../reducers/allReducers';

let theStore = createStore(allReducers, initialState, applyMiddleware(thunk));

export default theStore;
