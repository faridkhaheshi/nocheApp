import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import ViewContainer from '../components/ViewContainer';
import TopPlaceholder from '../components/TopPlaceholder';
import styles from '../styles/styles';
import constants from '../../constants';



class WelcomeScreen extends Component{
  /*
    We have the following props here:
    {
      navigator,
      appState,
      appActions,
      serverConnection,
      resourceMap,
    }
  */

  constructor(props){
    super(props);
  }

  componentDidMount(){

    let appState = this.props.appState;
    let appActions = this.props.appActions;

    if(appState.clientInfo.status !== constants.clientStates.OK){
      // make it OK.

      if(appState.clientInfo.status === constants.clientStates.INVALID){
        appActions.solveTokenValidityProblem();
      }
      else if(appState.clientInfo.status !== constants.clientStates.INITIALIZING){
        appActions.initializeClient();
      }
    }
    else if(appState.connectionInfo.subscriptionStatus === constants.subscriptionStates.NOT_SUBSCRIBED){
      // subscribe.
      let connectionInfo = {
        clientToken: this.props.appState.clientInfo.token,
        channels: this.props.appState.clientInfo.channels
      };

      appActions.establishServerConnections(this.props.serverConnection, connectionInfo, this.props.resourceMap);
    }
    else if(appState.connectionInfo.subscriptionStatus === constants.subscriptionStates.FAILED){
      appActions.solveTokenValidityProblem();
    }

  }

  componentDidUpdate(prevProps, prevState){


    let appState = this.props.appState;
    let appActions = this.props.appActions;

    if(appState.clientInfo.status !== constants.clientStates.OK){
      // make it OK.
      if(appState.clientInfo.status === constants.clientStates.INVALID){
        appActions.solveTokenValidityProblem();
      }
      else if(appState.clientInfo.status !== constants.clientStates.INITIALIZING){
        appActions.initializeClient();
      }
    }
    else if(appState.connectionInfo.subscriptionStatus === constants.subscriptionStates.NOT_SUBSCRIBED){
      // subscribe.

      let connectionInfo = {
        clientToken: this.props.appState.clientInfo.token,
        channels: this.props.appState.clientInfo.channels
      };

      appActions.establishServerConnections(this.props.serverConnection, connectionInfo, this.props.resourceMap);
    }
    else if(appState.connectionInfo.subscriptionStatus === constants.subscriptionStates.FAILED){
      appActions.solveTokenValidityProblem();
    }

  }



  render(){


    let welcomeText = 'سلام!';

    switch(this.props.appState.clientInfo.status){
      case constants.clientStates.INITIALIZING:
        welcomeText = 'چند لحظه صبر کن وسایلم رو جمع کنم...';
        break;
      case constants.clientStates.NOT_INITIALIZIED:
        welcomeText = 'مشکلی پیش اومد. دوباره سعی کن...';
        break;
      default:
        welcomeText ='سلام!';
    }

    return(
      <View style={ styles.welcomeScreenPlaceholder }>
        <Image style={ styles.welcomeAvatar }
                source={require('../images/welcomeAvatar.png')} />
        <View style={ styles.welcomeScreenTextSpace }>
          <Text
            ref='welcomeScreenText'
            style={styles.welcomeScreenText}>
            {welcomeText}</Text>
        </View>
      </View>
    );

  }
}

export default WelcomeScreen;
