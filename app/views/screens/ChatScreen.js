import React, { Component } from 'react';
import { Text } from 'react-native';
import ViewContainer from '../components/ViewContainer';
import MessageList from '../components/MessageList';
import TopPlaceholder from '../components/TopPlaceholder';
import InputDesk from '../components/InputDesk';
import DialogBox from '../components/DialogBox';
import PhotoPickerModal from '../components/PhotoPickerModal';
import constants from '../../constants';
import MessageActionsMenu from '../components/MessageActionsMenu';

class ChatScreen extends Component{
  /*
    Here, we will have the following props:
    {
      navigator,
      appState,
      appActions,
      resourceMap,
    }
  */

  constructor(props){
    super(props);
  }

  componentDidMount(){
    let loadingState = this.props.appState.messages.loadingState;
    let appActions = this.props.appActions;
    let clientToken = this.props.appState.clientInfo.token;
    let messageMap = this.props.resourceMap.messages;


    if(loadingState === constants.messageLoadingStates.fresh){
      appActions.fetchLastMessages(clientToken, messageMap);
    }

  }

  render(){


    /*
    return(
      <ViewContainer appActions={{setDeviceState: this.props.appActions.setDeviceState}}>
        <TopPlaceholder />
        <Text>ChatScreen</Text>
      </ViewContainer>
    );
    */


    return(
      <ViewContainer appActions={{setDeviceState: this.props.appActions.setDeviceState}}>
        <TopPlaceholder />
        <DialogBox backgroundColor={'red'} alertState={this.props.appState.alertState}/>
        <MessageList
          messages={this.props.appState.messages}
          appActions={this.props.appActions}
          deviceState={this.props.appState.deviceState}
          navigator={this.props.navigator}
          clientToken={this.props.appState.clientInfo.token}
          resourceMap={this.props.resourceMap}
          connectionInfo={this.props.appState.connectionInfo}/>
        <InputDesk
          nextDraftIndex={this.props.appState.messages.nextDraftIndex}
          actions={{
            sendMessage: this.props.appActions.sendMessage,
            togglePhotoModalVisibility: this.props.appActions.togglePhotoModalVisibility
          }}
          clientToken={this.props.appState.clientInfo.token}
          resourceMap={this.props.resourceMap} />
        <PhotoPickerModal
          visible={this.props.appState.appElements.showPhotoModal}
          nextDraftIndex={this.props.appState.messages.nextDraftIndex}
          clientToken={this.props.appState.clientInfo.token}
          appActions={{
            sendMessage: this.props.appActions.sendMessage,
            togglePhotoModalVisibility: this.props.appActions.togglePhotoModalVisibility,
            setDeviceState: this.props.appActions.setDeviceState,
          }}
          deviceState={this.props.appState.deviceState}
          resourceMap={this.props.resourceMap} />
        <MessageActionsMenu
          selectedMessageKey={this.props.appState.messages.selectedMessageKey}
          resourceMap={this.props.resourceMap}
          appActions={this.props.appActions}/>
      </ViewContainer>
    );

  }
}

export default ChatScreen;
