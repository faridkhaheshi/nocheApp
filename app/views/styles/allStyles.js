import { StyleSheet } from 'react-native';
import messageStyles from './messageStyles';
import messageActionsStyles from './messageActionsStyles';
import containerStyles from './containerStyles';
import inputsStyles from './inputsStyles';
import constants from '../../constants';

export default {
  message: messageStyles,
  messageActions: messageActionsStyles,
  container: containerStyles,
  inputs: inputsStyles,
};
