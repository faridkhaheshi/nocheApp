import { StyleSheet, Dimensions } from 'react-native';
import constants from '../../constants';

const { width } = Dimensions.get('window');

const messageStyles = StyleSheet.create({
  userMessageBodyContainer:{
    maxWidth: '90%',
    alignSelf: 'flex-end',
    backgroundColor: '#ABEBC6',
    borderRadius: 10,
    overflow: 'hidden',
    margin: 5,
    padding: 3,
  },
  userMessageFooterTimeStamp:{
    fontFamily: 'Shabnam',
    fontSize: 10,
    textAlign: 'right',
    color: 'teal'
  },
  userMessageFooterSendStatus:{
    fontFamily: 'Shabnam',
    fontSize: 10,
    textAlign: 'right',
    color: 'teal'
  },
  botMessageBodyContainer:{
    maxWidth: '90%',
    alignSelf: 'flex-start',
    backgroundColor: 'white',
    borderRadius: 10,
    overflow: 'hidden',
    margin: 5,
    padding: 3,
  },
  botMessageFooterTimeStamp:{
    fontFamily: 'Shabnam',
    fontSize: 10,
    textAlign: 'right',
    color: 'silver'
  },
  botMessageFooterSendStatus:{
    fontFamily: 'Shabnam',
    fontSize: 10,
    textAlign: 'right',
    color: 'silver'
  },
  messageText:{
    fontFamily: 'Shabnam',
    fontSize: 12,
    textAlign: 'right',
    padding: 10,
  },
  messageFooter:{
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  messagePhoto:{
    borderRadius: 10,
    margin: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  messagePhotoProgressCircle:{
    backgroundColor: 'darkslategray',
    opacity: 0.9,
    borderRadius: 5,
    padding: 5,
  },
  messagePhotoProgressCircleTextContainer:{
    position: 'absolute',
    top: 5,
    left: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  messagePhotoProgressText:{
    fontFamily: 'Shabnam',
    fontSize: 16,
    textAlign: 'right',
    color: 'white'
  },
  imageUploadProgress:{

  },
  imagePlaceholder:{
    backgroundColor: 'lightgray',
  },
  noMessagePlaceholder: {
    backgroundColor: '#e0e0e0',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  noMessagePlaceholderPic:{

  },
  noConnectionPlaceholder:{
    backgroundColor: '#e0e0e0',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  errorMessagesText:{
    fontFamily: 'Shabnam',
    fontSize: 14,
    textAlign: 'right',
    color: 'grey',
  },
  actionableTextItems:{
    fontFamily: 'Shabnam',
    fontSize: 14,
    textAlign: 'right',
    color: 'blue',
    marginTop: 50,
  },
});


export default messageStyles;
