import { StyleSheet, Dimensions } from 'react-native';
import constants from '../../constants';

const { width } = Dimensions.get('window');

const containerStyles = StyleSheet.create({
  viewContainer:{
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "stretch",
  },
  viewContainerTop:{
    flex: 1,
  },
});


export default containerStyles;
