import { StyleSheet, Dimensions } from 'react-native';
import constants from '../../constants';

const { width } = Dimensions.get('window');

const messageActionsStyles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'flex-end',
    marginTop: 20,
    alignItems: 'center',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    width: '100%',
    height: '100%',
  },
  topButtonsContainer:{
    maxHeight: 120,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'white',
    marginBottom: 5,
    borderRadius: 5,
  },
  topButtonsListViewContainer:{
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  bottomButtonsListViewContainer:{
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  bottomButtonContainer:{
    maxHeight: 60,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'white',
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
    //padding: 5,
    //padding: 5,
  },
  Button:{
    height: 60,
    width: 0.9*width,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  buttonText:{
    fontFamily: 'Shabnam',
    fontSize: 16,
    textAlignVertical: 'center',
    textAlign: 'center',
    color: 'blue',
  },
  buttonSeparator:{
    backgroundColor: 'lightgrey',
    height: StyleSheet.hairlineWidth,
  },
});


export default messageActionsStyles;
