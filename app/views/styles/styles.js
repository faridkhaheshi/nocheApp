import { StyleSheet, Dimensions, Platform } from 'react-native';
import constants from '../../constants';

const { width } = Dimensions.get('window');
const topPlaceholderHeight = (Platform.OS === 'ios')?64:56;
const photoModalTopPadding = (Platform.OS === 'ios')?20:0;

const styles = StyleSheet.create({
  appNavigator:{},
  photoModalContainerPortrait: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 0,
    alignItems: 'stretch',
    justifyContent: 'flex-end',
  },
  photoModalContainerLandscape:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  cameraPreview:{
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  cameraContainerPortrait:{
    flexDirection: 'column',
  },
  cameraContainerLandscape:{
    flexDirection: 'row',
  },
  imageFinalizedContainerPortrait:{
    flexDirection: 'column',
  },
  imageFinalizedContainerLandscape:{
    flexDirection: 'row',
  },
  cameraControllerPortrait:{
    backgroundColor: '#191919',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  cameraControllerLandscape:{
    backgroundColor: '#191919',
    flexDirection: 'column-reverse',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  imageFinalizerControllerPortrait:{
    backgroundColor: '#191919',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  imageFinalizerControllerLandscape:{
    backgroundColor: '#191919',
    flexDirection: 'column-reverse',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  closeCameraIconContainer:{
    margin: 20,
  },
  closeCameraIcon:{
    color: 'white',
    opacity: 0.9,
  },
  captureIconContainer:{

  },
  captureIcon:{
    color: 'white',
    opacity: 0.9,
  },
  imageFinalizerContainer:{

  },
  closeImageFinalizerIconContainer:{
    margin: 20,
  },
  closeImageFinalizerIcon:{
    color: 'white',
    opacity: 0.9,
  },
  confirmImageIconContainer:{

  },
  confirmImageIcon:{
    color: 'white',
    opacity: 0.9,
  },
  retryPictureIconContainer:{
    margin: 20,
  },
  retryPictureIcon:{
    color: 'white',
    opacity: 0.9,
  },
  confirmPictureText:{
    backgroundColor: 'slategray',
    opacity: 0.9,
    fontFamily: 'Shabnam',
    fontSize: 18,
    color: 'white',
    padding: 10,
    marginBottom: 5,
    marginRight: 5,
  },
  retryPictyreIcon:{
    color: 'white',
    backgroundColor: 'slategray',
    opacity: 0.9,
    width: 40,
    height: 40,
    alignSelf: 'flex-end',
    padding: 5,
    marginLeft: 5,
    marginBottom: 5,
  },
  sendPhotoButton:{
    position: 'absolute',
    width: width,
    padding: 10,
    bottom: 0,
    left: 0,
    height: 60,
  },
  photoModalButton:{
    height: 60,
  },
  nocheButtonTextDefault:{
    fontFamily: 'Shabnam',
    fontSize: 14,
  },
  nocheButton:{
    height: 40,
    alignItems: 'center',
    justifyContent: 'center'
  },
  photoModalButtonTextStyle:{
    color: 'darkblue',
    fontSize: 18,
  },
  statusBarBackground: {},
  navigationBar: {
    backgroundColor: 'whitesmoke',
    borderBottomColor: 'silver',
    borderBottomWidth: 0.5
  },
  navigatorBackIcon:{
    color: "grey",
    paddingLeft: 10,
    marginTop: 15
  },
  navigationBarText:{
    paddingTop: 15,
    fontFamily: 'Shabnam',
    fontSize: 14,
  },
  nocheAvatar:{
    marginTop: 10,
    marginRight: 10,
    width: 30,
    height: 30,
  },
  topPlaceholder:{
    height: topPlaceholderHeight,
  },
  dialogBox:{
    height: 18,
    backgroundColor: 'red',
  },
  dialogBoxText:{
    fontFamily: 'Shabnam',
    fontSize: 10,
    textAlign: 'center',
    lineHeight: 18,
  },
  loadingPageContainer:{
    backgroundColor: constants.colors.baseBackground,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loadingPageActivityIndicator:{

  },
  noMessagePlaceholder: {
    backgroundColor: 'blue',
    flex: 1,
  },
  viewContainer:{
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "stretch"
  },
  welcomeScreenPlaceholder:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor: '#EBF5FB'
  },
  welcomeScreenTextSpace:{
    flex: 1,
  },
  welcomeScreenText:{
    fontFamily: 'Shabnam',
    fontSize: 16,
    textAlign: 'right',
  },
  welcomeAvatar:{
    width: 200,
    resizeMode: 'contain',
    flex: 1,
    paddingTop: 250
  },
  messageList:{
    backgroundColor: constants.colors.baseBackground,
    flex: 1
  },
  inlineKeyboardContainer:{
  },
  inlineKeyboard:{
    alignItems: 'stretch',
    alignSelf: 'center',
    minWidth: 200
  },
  inlineKey:{
    backgroundColor: 'rgba(128,128,144,0.4)',
    marginTop: 2,
    marginBottom: 2,
    borderRadius: 3,
    marginRight: 5,
    marginLeft: 5,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    height: 30,
  },
  inlineKeyText:{
    fontFamily: 'Shabnam',
    fontSize: 12,
    textAlign: 'center',
    color: 'white',
    lineHeight: 30,
  },
  retryIcon:{
    color: 'white'
  },
  inputDesk:{
    overflow: 'hidden',
    flexDirection: 'column',
    backgroundColor: 'whitesmoke',
    borderTopColor: 'silver',
    borderTopWidth: 0.5
  },
  inputsRow:{
  	flexDirection: 'row',
  	backgroundColor: 'whitesmoke',
  	alignItems: 'flex-end',
  	justifyContent: 'space-between'
  },
  paperClipIcon:{
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 13,
    maxWidth: 60,
  	color: 'grey',
  	transform: [{scaleX: -1}]
  },
  nocheTextInput:{
    marginTop: 8,
    marginBottom: 8,
    alignSelf: 'center',
    height: 28,
    backgroundColor: 'white',
    borderColor: 'silver',
    borderWidth: 0.5,
    paddingRight: 10,
    borderRadius: 3,
    overflow: 'hidden',
    fontSize: 12,
    fontFamily: 'Shabnam',
    textAlign: 'right',
    color: 'grey',
    flexGrow: 1
  },
  sendInputText: {
    maxWidth: 100,
    fontFamily: "Shabnam",
    fontSize: 14,
    color: 'dodgerblue',
    paddingRight: 10,
    paddingLeft: 10,
    marginBottom: 13
  },
  quickInputsDesk:{
    maxHeight: 182,
  	flexDirection: 'column',
  	backgroundColor: 'gainsboro',
    paddingTop: 4,
    paddingBottom: 4
  },
  quickInput:{
    fontSize: 12,
    fontFamily: 'Shabnam',
    height: 33,
    lineHeight: 33,
    textAlignVertical: 'center',
    color: 'dimgray',
    backgroundColor: 'whitesmoke',
    marginVertical: 4,
    marginHorizontal: 8,
    borderRadius: 3,
    overflow: 'hidden',
    textAlign: 'center'
  },

});

export default styles;
