import { StyleSheet } from 'react-native';
import constants from '../../constants';

const inputsStyles = StyleSheet.create({
  nocheTextInputIOS:{
    marginTop: 8,
    marginBottom: 8,
    padding: 0,
    alignSelf: 'center',
    height: constants.appParams.textInputDefaultHeight,
    backgroundColor: 'white',
    borderColor: 'silver',
    borderWidth: 0.5,
    paddingRight: 10,
    borderRadius: 3,
    overflow: 'hidden',
    fontSize: 12,
    fontFamily: 'Shabnam',
    textAlign: 'right',
    color: 'grey',
    flexGrow: 1,
  },
  nocheTextInputAndroid:{
    flex: 1,
    marginTop: 8,
    marginBottom: 8,
    alignSelf: 'center',
    padding: 0,
    height: constants.appParams.textInputDefaultHeight,
    backgroundColor: 'white',
    //backgroundColor: 'yellow',
    borderColor: 'silver',
    borderWidth: 0.5,
    paddingRight: 10,
    borderRadius: 3,
    overflow: 'hidden',
    fontSize: 12,
    fontFamily: 'Shabnam',
    textAlign: 'right',
    color: 'grey',
    flexGrow: 1,
    textAlignVertical: 'center',
  },
  quickInputsDesk:{
    maxHeight: 182,
  	flexDirection: 'column',
  	backgroundColor: 'gainsboro',
    paddingTop: 4,
    paddingBottom: 4
  },
  quickInput:{
    alignItems: 'center',
    justifyContent: 'center',
    height: constants.appParams.quickInputLineHeight,
    backgroundColor: 'whitesmoke',
    marginVertical: 4,
    marginHorizontal: 8,
    borderRadius: 3,
    overflow: 'hidden',
  },
  quickInputText:{
    fontSize: 12,
    fontFamily: 'Shabnam',
    textAlignVertical: 'center',
    color: 'dimgray',
    textAlign: 'center',
  },
  inlineKey:{
    backgroundColor: 'rgba(128,128,144,0.4)',
    marginTop: 2,
    marginBottom: 2,
    borderRadius: 3,
    marginRight: 5,
    marginLeft: 5,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    height: 30,
  },
  inlineKeyText:{
    fontFamily: 'Shabnam',
    fontSize: 12,
    textAlign: 'center',
    color: 'white',
    lineHeight: 30,
  },
  retryIcon:{
    color: 'white'
  },
  inlineKeyboardContainer:{
  },
  inlineKeyboard:{
    alignItems: 'stretch',
    alignSelf: 'center',
    minWidth: 200
  },
});


export default inputsStyles;
