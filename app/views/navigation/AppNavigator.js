'use strict'
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Navigator,
  Platform,
} from 'react-native';
import styles from '../styles/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import ChatScreen from '../screens/ChatScreen';
import WelcomeScreen from '../screens/WelcomeScreen';
import constants from '../../constants';


const navigationBarRouteMapper = (Platform.OS === 'ios')?
  {
    LeftButton: (route, navigator, index, navState) => {
      return null;
    },
    Title: (route, navigator, index, navState) => {
      return <Text style={styles.navigationBarText}>نوچه</Text>;
    },
    RightButton: (route, navigator, index, navState) => {
      return <Image source={require('../images/nocheAvatar.png')} style={styles.nocheAvatar}/>;
    }
  }
  :
  {
    LeftButton: (route, navigator, index, navState) => {
      return <Image source={require('../images/nocheAvatar.png')} style={styles.nocheAvatar}/>;
    },
    Title: (route, navigator, index, navState) => {
      return <Text style={styles.navigationBarText}>نوچه</Text>;
    },
    RightButton: (route, navigator, index, navState) => {
      return null;
    }
  };

class AppNavigator extends Component {

  /*
    Here, we have the following props:
    {
      appState,
      ...appActions,
      serverConnection,
      resourceMap,
    }
  */

  static propTypes = {};

  static defaultProps = {};


  constructor(props){
    super(props);

    this._renderScene = this._renderScene.bind(this);

  }

  render() {
    const routes = [
                    {ident: constants.screenNames.Welcome},
                    {ident: constants.screenNames.Chat},
                    ];
    let initialRoute = routes[0];
    let navBar = null;
    if( this.props.appState.clientInfo.status === constants.clientStates.OK
     && this.props.appState.connectionInfo.subscriptionStatus === constants.subscriptionStates.SUBSCRIBED){
      navBar = <Navigator.NavigationBar
                     style={styles.navigationBar}
                     routeMapper={navigationBarRouteMapper}/>;
    }


    return (
      <Navigator
        initialRoute={ initialRoute }
        ref="appNavigator"
        style={styles.appNavigator}
        renderScene={this._renderScene}
        configureScene={(route) => ({...route.sceneConfig || Navigator.SceneConfigs.FloatFromRight})}
        navigationBar={ navBar }
         {...this.props}/>
    );


  }

  _renderScene(route, navigator){
    // we have the pusher @ this.pusher
    const globalNavigatorProps = {navigator};

    const appState = this.props.appState;
    const resourceMap = this.props.resourceMap;
    const appActions = {
      sendMessage: this.props.sendMessage,
      openWebURL: this.props.openWebURL,
      sendCallbackData: this.props.sendCallbackData,
      initializeClient: this.props.initializeClient,
      setClientInfo: this.props.setClientInfo,
      fetchLastMessages: this.props.fetchLastMessages,
      loadMoreMessages: this.props.loadMoreMessages,
      togglePhotoModalVisibility: this.props.togglePhotoModalVisibility,
      setDeviceState: this.props.setDeviceState,
      loadImage: this.props.loadImage,
      addPhoto: this.props.addPhoto,
      selectMessage: this.props.selectMessage,
      disableScrollToBottom: this.props.disableScrollToBottom,
      solveTokenValidityProblem: this.props.solveTokenValidityProblem,
      establishServerConnections: this.props.establishServerConnections,
    };

    if(   appState.clientInfo.status === constants.clientStates.OK
      &&  appState.connectionInfo.subscriptionStatus === constants.subscriptionStates.SUBSCRIBED){
        return(
          <ChatScreen
            navigator={navigator}
            appState={appState}
            appActions={appActions}
            resourceMap={resourceMap} />
        );
    }
    else{
      return <WelcomeScreen
                navigator={navigator}
                appState={appState}
                appActions={appActions}
                serverConnection={this.props.serverConnection}
                resourceMap={resourceMap} />;
    }
  }




}

export default AppNavigator;
