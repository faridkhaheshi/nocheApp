import React, { Component, } from 'react';
import { View, Image, Dimensions } from 'react-native';
import allStyles from '../styles/allStyles';

const { width } = Dimensions.get('window');

class NoMessagePlaceholder extends Component {

  render() {
    return (
      <View style={allStyles.message.noMessagePlaceholder}>
        <Image
          style={[allStyles.message.noMessagePlaceholderPic ,{width: width, height: width}]}
          source={require('../images/noMessagePlaceholder.png')}
          resizeMode='center'/>
      </View>
    );
  }
}

export default NoMessagePlaceholder;
