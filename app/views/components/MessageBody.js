'use strict'
import React, { Component } from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import MessagePhoto from './MessagePhoto';
import MessageText from './MessageText';
import MessageFooter from './MessageFooter';
import allStyles from '../styles/allStyles';
import constants from '../../constants';


class MessageBody extends Component{
  /*
    Here, we will have the following props:
    {
      messageKey,
      photo,
      text,
      photoMap,
      appActions,
      timeStamp,
      status,
      sender,     // USER, BOT
      deviceState,
    }
  */

  constructor(props){
    super(props);

    this._onLongPress = this._onLongPress.bind(this);
  }

  render(){
    let bodyContainerStyle = (this.props.sender === constants.messageSenders.USER)?
                    allStyles.message.userMessageBodyContainer:
                    allStyles.message.botMessageBodyContainer;

    if(this.props.photo){
      return(
        <TouchableHighlight
          delayLongPress={400}
          onLongPress={this._onLongPress}
          underlayColor='rgba(51, 204, 255, 0.5)'>
          <View style={bodyContainerStyle}>
            <MessagePhoto
              photo={this.props.photo}
              deviceState={this.props.deviceState}
              appActions={this.props.appActions}
              photoMap={this.props.photoMap}/>
            <MessageText text={this.props.text} />
            <MessageFooter
              timeStamp={this.props.timeStamp}
              status={this.props.status}
              sender={this.props.sender} />
          </View>
        </TouchableHighlight>
      );
    }
    else{
      return(
        <View style={bodyContainerStyle}>
          <MessageText text={this.props.text} />
          <MessageFooter
            timeStamp={this.props.timeStamp}
            status={this.props.status}
            sender={this.props.sender} />
        </View>
      );
    }
  }

  _onLongPress(){
    //console.log(`messageKey: ${this.props.messageKey}`);
    this.props.appActions.selectMessage(this.props.messageKey);
  }
}

export default MessageBody;
