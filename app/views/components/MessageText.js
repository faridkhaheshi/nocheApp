'use strict'
import React, { Component } from 'react';
import { Text } from 'react-native';
import allStyles from '../styles/allStyles';

class MessageText extends Component{
  /*
    Here, we will have the following props:
    {
      text,
    }
  */

  render(){
    let messageText = this.props.text;
    if(messageText){
      return <Text style={allStyles.message.messageText}>{ messageText }</Text>;
    }
    else{
      return null;
    }
  }
}

export default MessageText;
