'use strict'
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import styles from '../styles/styles';
import allStyles from '../styles/allStyles';
import constants from '../../constants';
import * as utils from '../../utils/utils';
import InlineKeyboard from './InlineKeyboard';
import MessageBody from './MessageBody';

class Message extends Component{
  /*
    Here, we have the following props:
    {
      message,
      messageKey,
      photoMap,
      deviceState,
      appActions,
      navigator,
    }
  */

  constructor(props){
    super(props);
    this.state = {};

  }


  render(){
    /*
      Each message has the following properties:
      {
        text: 'some text',
        sender: User | BOT,
        message_index: someNumber,
        state: constants.messageStates,
        time_stamp: a number,
        inline_keyboards: [array of inline keyboards]
      }
    */

    let theMessage = this.props.message;
    let messageText = theMessage.text;
    let photo = (theMessage.photo || {});
    let photoMap = this.props.photoMap;

    return(
      <View>
        <MessageBody
          messageKey={this.props.messageKey}
          photo={photo}
          text={messageText}
          photoMap={photoMap}
          appActions={this.props.appActions}
          timeStamp={theMessage.time_stamp}
          status={theMessage.state}
          sender={theMessage.sender}
          deviceState={this.props.deviceState} />
        <InlineKeyboard
          inlineKeyboard={theMessage.inline_keyboard}
          messageIndex={theMessage.message_index}
          appActions={this.props.appActions}
          navigator={this.props.navigator}
          deviceState={this.props.deviceState} />
      </View>
    );

  }
}

export default Message;
