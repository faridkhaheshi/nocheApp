import React, { Component } from 'react';
import {
  Modal,
  Text,
  View,
  TouchableOpacity,
  ListView,
  TouchableWithoutFeedback,
  Clipboard,
  Share,
  Platform,
  Linking,
} from 'react-native';
import allStyles from '../styles/allStyles';


class MessageActionsMenu extends Component{

  /*
    we have the following props:
    {
      selectedMessageKey,
      appActions,
      resourceMap,
    }
  */
  constructor(props){
    super(props);

    this._onCancel = this._onCancel.bind(this);
    this._createButtons = this._createButtons.bind(this);
    this._onPressCopy = this._onPressCopy.bind(this);
    this._onPressShare = this._onPressShare.bind(this);
//    this._onPressShareViaTelegram = this._onPressShareViaTelegram.bind(this);

    this.menuItems = [
      {
        text: 'کپی کردن متن',
        onPressHandler: this._onPressCopy,
      },
      {
        text: 'ارسال برای دیگران',
        onPressHandler: this._onPressShare,
      },
      /*
      {
        text: 'ارسال برای دیگران (تلگرام)',
        onPressHandler: this._onPressShareViaTelegram,
      },
      */
    ];

    this.bottomButtons = [
      {
        text: 'لغو',
        onPressHandler: this._onCancel,
      },
    ];

    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      topButtonsDataSource: ds.cloneWithRows(this.menuItems),
      bottomButtonsDataSource: ds.cloneWithRows(this.bottomButtons),
    };
  }

  _onPressCopy(){
    let copyMessage = this.props.appActions.copyMessage;
    let theMessage = this.props.resourceMap.messages[this.props.selectedMessageKey];

    this._closeModal();
    Clipboard.setString(theMessage.text);
    //TODO: show an alert that the text has been copied to clipboard.
  }

  _logToConsole(){
    console.log('button pressed!!!');
  }

  _onPressShare(){

    let shareMessage = this.props.appActions.shareMessage;
    let theMessage = this.props.resourceMap.messages[this.props.selectedMessageKey];
    let thePhoto = theMessage.photo || {};
    let shareTitle = 'ببین نوچه ام چی میگه';

    let content = {
      message: theMessage.text,
      uri: thePhoto.uri,
      title: shareTitle,
    };

    Share
      .share(content)
      .then((shareResult) => {
        console.log('share resolved with:');
        console.log(shareResult);
        this._closeModal();
      })
      .catch((error) => {
        console.log('error happened:');
        console.log(error);
        this._closeModal();
      });


    /*
    shareMessage(theMessage);
    this._closeModal();
    */
  }


  _onShow(){

  }

  _onCancel(){
    //Let's deselect the message to get close the modal.
    this.props.appActions.selectMessage();
  }

  _closeModal(){
    //Let's deselect the message to get close the modal.
    this.props.appActions.selectMessage();
  }

  /*
    This method is responsible for rendering top buttons in the message actions menu.
    The bottom button will be a cancel button.
    Above the cancel button, other buttons will be available to user.
  */
  _createButtons(menuItem, sectionID, rowID, highlightRow){
    let separator = null;
    if(rowID > 0){
      return (
        <TouchableOpacity onPress={menuItem.onPressHandler}>
          <View style={allStyles.messageActions.buttonSeparator} />
          <View style={allStyles.messageActions.Button}>
            <Text style={allStyles.messageActions.buttonText}>
              {menuItem.text}
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
    else{
      return(
        <TouchableOpacity onPress={menuItem.onPressHandler}>
          <View style={allStyles.messageActions.Button}>
            <Text style={allStyles.messageActions.buttonText}>
              {menuItem.text}
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
  }


  render(){
    let visibility = false;;
    if(this.props.selectedMessageKey){
      visibility = true;
    }

    return(
        <Modal
            animationType={'fade'}
            transparent={true}
            visible={visibility}
            onShow={this._onShow}
            supportedOrientations={[ 'portrait', 'landscape', 'landscape-right', 'landscape-left' ]}>
            <TouchableWithoutFeedback onPress={this._onCancel}>
              <View style={allStyles.messageActions.container}>
                <View style={allStyles.messageActions.topButtonsContainer}>
                  <ListView
                    contentContainerStyle={allStyles.messageActions.topButtonsListViewContainer}
                    dataSource={this.state.topButtonsDataSource}
                    renderRow={this._createButtons}/>
                </View>
                <View style={allStyles.messageActions.bottomButtonContainer}>
                  <ListView
                    contentContainerStyle={allStyles.messageActions.topButtonsListViewContainer}
                    dataSource={this.state.bottomButtonsDataSource}
                    renderRow={this._createButtons} />
                </View>
              </View>
            </TouchableWithoutFeedback>
        </Modal>
    );
  }
}



export default MessageActionsMenu;
