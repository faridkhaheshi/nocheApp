import React, { Component } from 'react';
import {
  Image,
  Text,
  View,
  TouchableHighlight,
  ListView,
  CameraRoll,
} from 'react-native';
import styles from '../styles/styles';
import constants from '../../constants';


const ALBUM_PAGE_SIZE = 4;

/*
  We have the following props:
  {
    piecesLength,
    isPortrait,
    selectImage,
  }
*/

class ImagesAlbum extends Component{

  constructor(props){
    super(props);
    this._getImages = this._getImages.bind(this);
    this._renderImage = this._renderImage.bind(this);
    this._onEndReached = this._onEndReached.bind(this);
    this._selectImage = this._selectImage.bind(this);

    this.state = {
      images: [],
      selectedImageIndex: null,
      lastPageInfo:{
        has_next_page: true,
        end_cursor: null
      },
      nextPage:{
        first: ALBUM_PAGE_SIZE,
        after: null,
        askedFor: false,
      },
    };

  }

  componentWillReceiveProps(nextProps){
    if(nextProps.selectedImageIndex === 0){
      this.setState({
        selectedImageIndex: nextProps.selectedImageIndex,
        images: [],
        lastPageInfo:{
          has_next_page: true,
          end_cursor: null,
        },
        nextPage:{
          first: ALBUM_PAGE_SIZE,
          after: null,
          askedFor: false,
        },
      });
    }

  }

  _getImages(){

    let getPhotosOptions = {
      first: this.state.nextPage.first,
    };

    if(this.state.nextPage.after){
      getPhotosOptions.after = this.state.nextPage.after;
    }

    CameraRoll.getPhotos(getPhotosOptions)
      .then((r) => {
        /*
          TODO: ensure that the after property in getPhotoOptions is
                identical to the same field in this.state.nextPage.after
        */

        if(this.state.nextPage.after == getPhotosOptions.after){
          let pageInfo = r.page_info;
          let newImages = r.edges;
          let oldImages = this.state.images;

          let allImages = oldImages.concat(newImages);
          let newState = {
            images: allImages,
            lastPageInfo: pageInfo,
          };
          if(pageInfo.has_next_page){
            newState.nextPage = {
              first: ALBUM_PAGE_SIZE,
              after: pageInfo.end_cursor,
              askedFor: false,
            };
          }

          this.setState(newState);
        }

      })
      .catch((error) => {
        console.log(error);
      })
  }

  /*
    This method loads more images from CameraRoll when the end is reached.
  */
  _onEndReached(){
    let askedFor = (this.state.nextPage || {}).askedFor;
    if(this.state.lastPageInfo.has_next_page && !askedFor){
      this.setState({
        nextPage:{
          first: ALBUM_PAGE_SIZE,
          after: this.state.lastPageInfo.end_cursor,
          askedFor: true,
        },
      });
      this._getImages();
    }
  }

  _selectImage(someIndex, theImage){
    if (someIndex === this.state.index) {
      someIndex = null;
    }

    this.setState({selectedImageIndex: someIndex});


    if(someIndex){
      this.props.selectImage({uri: theImage.node.image.uri}, someIndex);
    }
    else{
      this.props.selectImage(null, null);
    }
  }

  _renderImage(image, sectionID, imageIndex, highlightImage){
    let piecesLength = this.props.piecesLength;
    return(
      <TouchableHighlight
        style={{opacity: imageIndex === this.state.selectedImageIndex ? 0.5 : 1}}
        key={imageIndex}
        underlayColor='transparent'
        onPress={()=>{this._selectImage(imageIndex, image)}} >
        <Image
          style={{
            width: piecesLength,
            height: piecesLength,
          }}
          source={{uri: image.node.image.uri}} />
      </ TouchableHighlight>
    );
  }

  render(){
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    let piecesLength = this.props.piecesLength;
    let containerStyle = this.props.isPortrait?{height: piecesLength}:{width: piecesLength};
    return (
      <View style={containerStyle}>
        <ListView
          ref='imagesAlbumListView'
          dataSource={ds.cloneWithRows(this.state.images)}
          renderRow={this._renderImage}
          onEndReached={this._onEndReached}
          enableEmptySections={true}
          horizontal={this.props.isPortrait}
        />
      </View>
    );
  }
}

export default ImagesAlbum;
