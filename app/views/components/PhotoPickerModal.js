import React, { Component } from 'react';
import {
  Text,
  Modal,
  View,
  Button,
  CameraRoll,
  ScrollView,
  TouchableHighlight,
  Image,
  Platform,
} from 'react-native';
import styles from '../styles/styles';
import constants from '../../constants';
import NocheButton from './NocheButton';
import ImagesAlbum from './ImagesAlbum';
import Camera from './Camera';
import ImageFinalizer from './ImageFinalizer';
import * as utils from '../../utils/utils';

class PhotoPickerModal extends Component{

  static propTypes = {}
  static defaultProps = {};

  /*
    We will have the following props here:
    appActions: {
      sendMessage,
      togglePhotoModalVisibility,
      setDeviceState,
    },
    visible: true | false,
    deviceState: {
      width:
      height:
      isPortrait:
    },
    nextDraftIndex: ,
    clientToken: ,
    resourceMap,
  */

  constructor(props) {
    super(props);

    let currentOrientation = props.deviceState.isPortrait ? 'portrait': 'landscape';

    this._toggleModal = this._toggleModal.bind(this);
    this._sendPhoto = this._sendPhoto.bind(this);
    this._selectImage = this._selectImage.bind(this);
    this._startAgain = this._startAgain.bind(this);
    this._onOrientationChange = this._onOrientationChange.bind(this);
    this._onCapturePhoto = this._onCapturePhoto.bind(this);
    this._onShow = this._onShow.bind(this);

    this.state = {
      selectedImage: null,
      selectedImageIndex: null,
      orientation: currentOrientation,
    };
  }

  _sendPhoto(){
    let sendMessage = this.props.appActions.sendMessage;
    let clientToken = this.props.clientToken;
    let nextDraftIndex = this.props.nextDraftIndex;
    let resourceMap = this.props.resourceMap;
    let messageList = this.props.messageList;

    Image.getSize(this.state.selectedImage.uri, (width, height) => {

      utils.reduceImageSize({
          uri: this.state.selectedImage.uri,
          width,
          height,
        })
        .then((newImage) => {
          /*
            Here, we have the new image with reduced size. let's send it.
            It has the following form:
            {
              uri,
              width,
              height,
            }
          */
          sendMessage({
            photo:{ local_uri: newImage.uri }
          }, clientToken, nextDraftIndex, resourceMap);
        })
        .catch((error) => {
          // TODO: handle this error.
        });

    }, (error) => {
      // unable to get image width and height.
      //TODO: handle this error.
      console.log(error);
    });

    this.props.appActions.togglePhotoModalVisibility();

  }

  _startAgain(){
    this.setState({
      selectedImage: null
    });
  }

  _toggleModal(){
    this.props.appActions.togglePhotoModalVisibility();
  }


  _selectImage(someImage, imageIndex){
    this.setState({
      selectedImage: someImage,
      selectedImageIndex: imageIndex,
    });
  }

  _onOrientationChange(event){
    let oldWidth = this.props.deviceState.width;
    let oldHeight = this.props.deviceState.height;


    if(event.nativeEvent.orientation === 'portrait'){

      this.props.appActions.setDeviceState({
        width: Math.min(oldWidth, oldHeight),
        height: Math.max(oldWidth, oldHeight),
        isPortrait: true,
      });

    }
    else{
      this.props.appActions.setDeviceState({
        width: Math.max(oldWidth, oldHeight),
        height: Math.min(oldWidth, oldHeight),
        isPortrait: false,
      });
    }
  }

  _onCapturePhoto(someImage){
    this.setState({
      selectedImage: someImage,
      selectedImageIndex: 0,
    });
  }

  _onShow(){

    this.setState({
      selectedImage: null,
      selectedImageIndex: null,
    });
  }

  render(){
    let width = this.props.deviceState.width;
    let height = this.props.deviceState.height;
    let isPortrait = this.props.deviceState.isPortrait;
    let cameraEdge = Math.min(width, height);
    let topSpace = (Platform.OK === 'ios')?20:0;
    let imageAlbumPiecesLength = cameraEdge / 3;
    let cameraControllerHeight = (isPortrait)?(height - 4*cameraEdge/3 - 20):(width - 4*cameraEdge/3);

    if(!isPortrait){
      //for landscape:

      if((width - cameraEdge) < (2*cameraEdge/3)){
        imageAlbumPiecesLength = (width - cameraEdge)/2;
        cameraControllerHeight = (width - cameraEdge)/2;
      }
      else{
        imageAlbumPiecesLength = cameraEdge/3;
        cameraControllerHeight = width - 4*cameraEdge/3;
      }
    }
    else if((height - topSpace - cameraEdge)<(2*cameraEdge/3)){
      // if we cannot allocate width/3 to album image pieces:
      imageAlbumPiecesLength = (height - cameraEdge - topSpace)/2;
      cameraControllerHeight = (height - cameraEdge - topSpace)/2;
    }

    let photoModalContainerStyle = (isPortrait)?(styles.photoModalContainerPortrait):(styles.photoModalContainerLandscape);

    return(
      <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.props.visible}
          onRequestClose={() => {
            console.log('closed');
          }}
          onShow={this._onShow}
          supportedOrientations={[ 'portrait', 'landscape', 'landscape-right', 'landscape-left' ]}
          onOrientationChange={this._onOrientationChange}
          style={{backgroundColor: 'red'}}
        >
        <View style={photoModalContainerStyle}>
          <ImagesAlbum
            piecesLength={imageAlbumPiecesLength}
            isPortrait={isPortrait}
            selectedImageIndex={this.state.selectedImageIndex}
            selectImage={this._selectImage} />

          {
            (this.state.selectedImage)?(
              <ImageFinalizer
                width={cameraEdge}
                height={cameraEdge}
                controllerHeight={cameraControllerHeight}
                isPortrait={isPortrait}
                source={{uri: this.state.selectedImage.uri}}
                onConfirm={this._sendPhoto}
                onRetry={this._startAgain}
                onClose={this.props.appActions.togglePhotoModalVisibility}
                />
            ):(
              <Camera
                width={cameraEdge}
                height={cameraEdge}
                controllerHeight={cameraControllerHeight}
                appActions={this.props.appActions}
                isPortrait={isPortrait}
                onCapture={this._onCapturePhoto} />
            )
          }
        </View>
      </Modal>
    );
/*
    return(
      <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.props.visible}
          onRequestClose={() => {
            console.log('closed');
          }}
          onShow={this._onShow}
          supportedOrientations={[ 'portrait', 'landscape', 'landscape-right', 'landscape-left' ]}
          onOrientationChange={this._onOrientationChange}
          style={{backgroundColor: 'red'}}
        >
        <View style={photoModalContainerStyle}>
          <ImagesAlbum
            piecesLength={imageAlbumPiecesLength}
            isPortrait={isPortrait}
            selectedImageIndex={this.state.selectedImageIndex}
            selectImage={this._selectImage} />

          {
            (this.state.selectedImage)?(
              <ImageFinalizer
                width={cameraEdge}
                height={cameraEdge}
                controllerHeight={cameraControllerHeight}
                isPortrait={isPortrait}
                source={{uri: this.state.selectedImage.uri}}
                onConfirm={this._sendPhoto}
                onRetry={this._startAgain}
                onClose={this.props.appActions.togglePhotoModalVisibility}
                />
            ):(
              <Camera
                width={cameraEdge}
                height={cameraEdge}
                controllerHeight={cameraControllerHeight}
                appActions={this.props.appActions}
                isPortrait={isPortrait}
                onCapture={this._onCapturePhoto} />
            )
          }
        </View>
      </Modal>
    );
    */
  }
}

export default PhotoPickerModal;
