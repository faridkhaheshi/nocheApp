import React, { Component, } from 'react';
import { View, ActivityIndicator } from 'react-native';
import styles from '../styles/styles';

class LoadingPage extends Component {

  render() {
    return (
      <View style={styles.loadingPageContainer}>
        <ActivityIndicator
          animating={true}
          style={styles.loadingPageActivityIndicator}
          size="large"
        />
      </View>
    );
  }
}

export default LoadingPage;
