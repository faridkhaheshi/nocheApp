'use strict'
import React, { Component } from 'react';
import { Image, Animated, Text, View } from 'react-native';
import allStyles from '../styles/allStyles';
import * as utils from '../../utils/utils';
import constants from '../../constants';
import { AnimatedCircularProgress } from 'react-native-circular-progress';


class UploadingImage extends Component{
  /*
    Here, we will have the following props:
    {
      source,
      progress,
      uploadStatus,
      uploadRequest,
    }
  */

  constructor(props){
    super(props);
  }

  render(){
    return(
      <Image
        style={allStyles.message.messagePhoto}
        source={this.props.source}
        resizeMode={'cover'}>
        <AnimatedCircularProgress
          prefill={0}
          size={75}
          width={5}
          fill={this.props.progress}
          style={allStyles.message.messagePhotoProgressCircle}
          tintColor='white'
          backgroundColor='#2F4F4F'>
          {
            (fill) => (
              <View style={
                [
                  allStyles.message.messagePhotoProgressCircleTextContainer,
                  {
                    width: 75,
                    height: 75,
                  }
                ]
              }>
                <Text style={allStyles.message.messagePhotoProgressText}>
                  { `${this.props.progress}%`.convertNumbersToPersian() }
                </Text>
              </View>

            )
          }
        </AnimatedCircularProgress>
      </Image>
    );
  }

}

export default UploadingImage;
