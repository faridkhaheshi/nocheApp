'use strict'
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import allStyles from '../styles/allStyles';
import * as utils from '../../utils/utils';
import constants from '../../constants';


class MessageFooter extends Component{
  /*
    Here, we will have the following props:
    {
      timeStamp,
      status,
      sender,
    }
  */

  render(){
    let timeStampStyle = (this.props.sender === constants.messageSenders.USER)?
                  (allStyles.message.userMessageFooterTimeStamp):
                  (allStyles.message.botMessageFooterTimeStamp);
    let sendStatusStyle = (this.props.sender === constants.messageSenders.USER)?
                  (allStyles.message.userMessageFooterSendStatus):
                  (allStyles.message.botMessageFooterSendStatus);

    return (
      <View style={allStyles.message.messageFooter}>
        <Text style={ timeStampStyle }>
          { utils.translateTimeStamp(this.props.timeStamp) }
        </Text>
        <Text style={ sendStatusStyle }>
          { utils.translateMessageState(this.props.status) }
        </Text>
      </View>
    );
  }
}

export default MessageFooter;
