import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableHighlight,
} from 'react-native';

import RNCamera from 'react-native-camera';
import styles from '../styles/styles';
import Icon from 'react-native-vector-icons/FontAwesome';

/*
  We have the following props:
  {
    width,
    height,
    controllerHeight,
    appActions,
    isPortrait,
    onCapture,
  }

*/

class Camera extends Component{

  constructor(props){
    super(props);

    this._takePicture = this._takePicture.bind(this);

  }
  _takePicture(){
    this.camera.capture()
      .then((data) => {
        this.props.onCapture({uri: data.path});
        console.log(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }


  render(){
    let cameraStyles = styles.cameraPreview;
    if(this.props.width && this.props.height){
      cameraStyles = [styles.cameraPreview, {width: this.props.width, height: this.props.height}];
    }
    let cameraContainerStyle = this.props.isPortrait ? (styles.cameraContainerPortrait) : (styles.cameraContainerLandscape);
    let cameraControllerStyle = this.props.isPortrait ? (
      [styles.cameraControllerPortrait, {height: this.props.controllerHeight, width: '100%'}]
      ) : (
      [styles.cameraControllerLandscape, {width: this.props.controllerHeight, height: '100%'}]
      );

    return(
      <View style={cameraContainerStyle}>
        <RNCamera
          ref={(cam)=>{
            this.camera = cam;
          }}
          style={cameraStyles}
          aspect={RNCamera.constants.Aspect.fill}>
        </RNCamera>
        <View
          ref='cameraController'
          style={cameraControllerStyle}
          >

          <View style={styles.closeCameraIconContainer}>
            <TouchableHighlight
              underlayColor='transparent'
              onPress={this.props.appActions.togglePhotoModalVisibility}>
              <Icon name="close" size={30} style={styles.closeCameraIcon}/>
            </TouchableHighlight>
          </View>

          <View style={styles.captureIconContainer}>
            <TouchableHighlight
              underlayColor='transparent'
              onPress={this._takePicture}>
              <Icon name="circle" size={80} style={styles.captureIcon}/>
            </TouchableHighlight>
          </View>

          <View style={styles.retryPictureIconContainer}>
            <Icon name="refresh" size={30} style={{color: '#191919'}} />
          </View>

        </View>
      </View>
    );
  }

}


export default Camera;
