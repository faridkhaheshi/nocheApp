import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import styles from '../styles/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import NocheTextInput from './NocheTextInput';
import QuickInputsDesk from './QuickInputsDesk';
import constants from '../../constants';

class InputDesk extends Component{
  /*
    We have the following props:
    {
      nextDraftIndex,
      actions,
      clientToken,
      resourceMap,
    }
  */

  static defaultProps = {
    defaultMessages: [{text: 'سلام نوچه!'}]
  };

  render(){
    return(
      <View style={styles.inputDesk}>
        <View style={styles.inputsRow}>
          <TouchableOpacity onPress={(event) => {this._onPressAttach(event);}}>
            <Icon name="paperclip" size={20} style={styles.paperClipIcon}/>
          </TouchableOpacity>

          <NocheTextInput ref='nocheTextInput'/>
          <TouchableOpacity onPress={(event) => {this._onPressSend(event);}}>
            <Text style={styles.sendInputText}>ارسال</Text>
          </TouchableOpacity>
        </View>
        <QuickInputsDesk
          actions={this.props.actions}
          clientToken={this.props.clientToken}
          nextDraftIndex={this.props.nextDraftIndex}
          resourceMap={this.props.resourceMap} />
      </View>
    );
  }
  _onPressSend(event){
    const clientToken = this.props.clientToken;
    const sendMessage = this.props.actions.sendMessage;
    const nocheTextInput = this.refs.nocheTextInput;
    const nextDraftIndex = this.props.nextDraftIndex;
    const resourceMap = this.props.resourceMap;

    if(nocheTextInput.state.text.length > 0){
      sendMessage({text: nocheTextInput.state.text}, clientToken, nextDraftIndex, resourceMap);
      nocheTextInput.setState({text: '', height: 28});
      setTimeout(()=>{
        nocheTextInput.setState({text: constants.texts.textInputPlaceholderText, height: 28});
      }, 800);
    }
  }

  _onPressAttach(event){
    this.props.actions.togglePhotoModalVisibility();
  }

}


export default InputDesk;
