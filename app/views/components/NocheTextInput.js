import React, { Component } from 'react';
import { Text, View, TextInput, Alert, Platform } from 'react-native';
import allStyles from '../styles/allStyles';
import styles from '../styles/styles';
import constants from '../../constants';

const maxTextInputTextHeight = 140;

class NocheTextInput extends Component{

  constructor(props){
    super(props);
    this.state = {
      text: constants.texts.textInputPlaceholderText,
      height: constants.appParams.textInputDefaultHeight
    };
  }

  render(){
    let placeholderText = constants.texts.textInputPlaceholderText;

    const style = (Platform.OS === 'ios')?
                  allStyles.inputs.nocheTextInputIOS:
                  allStyles.inputs.nocheTextInputAndroid;


    return(
      <TextInput
        multiline={true}
        onChange={(event) => {
          this.setState({
            text: event.nativeEvent.text,
            height: event.nativeEvent.contentSize.height,
          });
        }}
        style = {[style, {height: Math.min(Math.max(this.state.height, constants.appParams.textInputDefaultHeight), maxTextInputTextHeight)}]}
        onChangeText={(text) => {this.setState({text});}}
        onFocus={() => {
          if(this.state.text == placeholderText){
            this.setState({text: ''});
          }
        }}
        onEndEditing={(e) => {if(e.nativeEvent.text == ''){this.setState({text: placeholderText, height: constants.appParams.textInputDefaultHeight});}}}
        value={this.state.text}
        underlineColorAndroid='transparent' />
    );
  }
}

export default NocheTextInput;
