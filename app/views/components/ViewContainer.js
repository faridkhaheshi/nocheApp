'use strict'
import React, { Component, } from 'react';
import { KeyboardAvoidingView, View, Platform } from 'react-native';
import allStyles from '../styles/allStyles';

class ViewContainer extends Component{


  constructor(props){
    super(props);

    this._onLayout = this._onLayout.bind(this);
  }

  componentWillMount(){

  }

  componentDidMount(){

  }

  _orientationDidChange(){
  }

  componentWillUnmount(){

  }

  render(){
    if(Platform.OS === 'ios'){
      return(
        <View
          style={allStyles.container.viewContainerTop}
          onLayout={this._onLayout}>
          <KeyboardAvoidingView
            style={allStyles.container.viewContainer}
            behavior='padding'>
              {this.props.children}
          </KeyboardAvoidingView>
        </View>
      );
    }
    else{
      return(
        <View
          style={allStyles.container.viewContainerTop}
          onLayout={this._onLayout}>
          <View
            style={allStyles.container.viewContainer}
            behavior='padding'>
              {this.props.children}
          </View>
        </View>
      );
    }

  }

  _onLayout(e){
    // we have the setDeviceState at this.props.appActions.setDeviceState

    let isPortrait = e.nativeEvent.layout.height > e.nativeEvent.layout.width;
    let setDeviceState = this.props.appActions.setDeviceState;
    setDeviceState({
      width: e.nativeEvent.layout.width,
      height: e.nativeEvent.layout.height,
      isPortrait
    });

  }

}

export default ViewContainer;
