'use strict'

import React, { Component } from 'react';
import { ListView, Text, View, TouchableOpacity, ActivityIndicator } from 'react-native';
import styles from '../styles/styles';
import constants from '../../constants';
import Icon from 'react-native-vector-icons/FontAwesome';
import SafariView from './SafariView';

class InlineKeyboard extends Component{
  /*
    Here, we have the following props:
    {
      inlineKeyboard,
      messageIndex,
      appActions,
      navigator,
      deviceState,
    }
  */

  static propTypes = {}

  static defaultProps = {
    inlineKeyboard: []
  };

  constructor(props){
    super(props);
  }

  render(){
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    return (
      <ListView
        contentContainerStyle={styles.inlineKeyboard}
        enableEmptySections={true}
        ref='inlineKeyboardsListView'
        dataSource={ds.cloneWithRows(this.props.inlineKeyboard)}
        renderRow={(inlineKey) => {return this._renderInlineKey(inlineKey)}}/>
    );
  }

  _renderInlineKey(inlineKey){
    //let content = <ActivityIndicator size='small' color='white'/>;
    let content = <Text style={styles.inlineKeyText}>{inlineKey.text}</Text>;
    if(inlineKey.status === constants.buttonStates.WAITING){
      content = <ActivityIndicator size='small' color='white'/>;
    }
    else if(inlineKey.status === constants.buttonStates.FAILED){
      content = <Icon name='refresh' size={20} style={styles.retryIcon}/>;
    }
    //<Text style={styles.inlineKey}>{inlineKey.text}</Text>;
    return (
      <TouchableOpacity onPress={()=>{this._serverInlineKey(inlineKey)}}>
        <View style={styles.inlineKey}>
          { content }
        </View>
      </TouchableOpacity>
    );
  }

  _serverInlineKey(someInlineKey){
    if(someInlineKey.status !== constants.buttonStates.WAITING){
      if(someInlineKey.url){
        this._openWebPage(someInlineKey.url);
      }
      else{
        let sendCallbackData = this.props.appActions.sendCallbackData;
        sendCallbackData(someInlineKey);
      }
    }
  }

  _openWebPage(url){
    SafariView.show({ url });
  }

}


export default InlineKeyboard;
