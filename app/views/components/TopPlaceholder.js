import React, { Component, } from 'react';
import { View, } from 'react-native';
import styles from '../styles/styles';

class TopPlaceholder extends Component {

  render() {
    return (
      <View style={[styles.topPlaceholder, {backgroundColor: this.props.backgroundColor}]}></View>
    );
  }
}

export default TopPlaceholder;