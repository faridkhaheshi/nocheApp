import React, { Component } from 'react';
import { Text, TouchableHighlight, View, Dimensions } from 'react-native';
import styles from '../styles/styles';

const { width } = Dimensions.get('window');


class NocheButton extends Component{

  static propTypes = {}

  static defaultProps = {};

  /*
    We will have the following props here:
    {
      title
      onPress
      style
    }
  */

  constructor(props) {
    super(props);
  }

  render(){
    return(
      <View style={[styles.nocheButton, this.props.style]}>
        <TouchableHighlight
            underlayColor='transparent'
            onPress={this.props.onPress}>
          <Text style={[styles.nocheButtonTextDefault, this.props.textStyle]}>
            {this.props.title}
          </Text>
        </TouchableHighlight>
      </View>
    );
  }
}

export default NocheButton;
