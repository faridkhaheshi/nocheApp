import React, { Component } from 'react';
import {
  Text,
  ListView,
  TouchableOpacity,
  Platform,
  View,
} from 'react-native';
import allStyles from '../styles/allStyles';


const defaultQuickInputs = [{text: 'سلام نوچه!'}];

class QuickInputsDesk extends Component{
  /*
    Here, we will have the following props:
    {
      actions,
      clientToken,
      nextDraftIndex,
      resourceMap,
    }
  */

  static propTypes = {}

  static defaultProps = {};

  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      quickInputs: ds.cloneWithRows(defaultQuickInputs),
    };
  }

  render(){

    return(
        <ListView
            style={allStyles.inputs.quickInputsDesk}
            dataSource={this.state.quickInputs}
            enableEmptySections={true}
            renderRow={(quickInput) => {return this._renderQuickInput(quickInput)}}>
        </ListView>

    );
  }

  _renderQuickInput(someQuickInput){
    let style = allStyles.inputs.quickInput;
    let textStyle = allStyles.inputs.quickInputText;
     return(
       <TouchableOpacity onPress={(event) => {this._sendQuickInput(event, someQuickInput.text)}}>
         <View style={style}>
           <Text style={textStyle}>{ someQuickInput.text }</Text>
         </View>
       </TouchableOpacity>
    );
  }

  _sendQuickInput(event, theQuickInput){
    const nextDraftIndex = this.props.nextDraftIndex;
    const sendMessage = this.props.actions.sendMessage;
    sendMessage({text: theQuickInput}, this.props.clientToken, nextDraftIndex, this.props.resourceMap);
  }

}

export default QuickInputsDesk;
