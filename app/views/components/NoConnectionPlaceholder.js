import React, { Component, } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import allStyles from '../styles/allStyles';

class NoConnectionPlaceholder extends Component {
  /*
    We have the following props:
    {
      fetchLastMessages,
      clientToken,
      messageMap,
    }
  */

  constructor(props){
    super(props);

    this._onPushedTryAgain = this._onPushedTryAgain.bind(this);
  }

  render() {
    return (
      <View style={allStyles.message.noConnectionPlaceholder}>
        <Text style={allStyles.message.errorMessagesText}>
        نتونستم به اینترنت وصل بشم.
        </Text>
        <Text style={allStyles.message.errorMessagesText}>
        مطمئنی اینترنت داری؟
        </Text>
        <TouchableOpacity onPress={this._onPushedTryAgain}>
          <Text style={allStyles.message.actionableTextItems}>
            یه بار دیگه امتحان کن
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  _onPushedTryAgain(){
    let clientToken = this.props.clientToken;
    let messageMap = this.props.messageMap;
    this.props.fetchLastMessages(clientToken, messageMap);
  }
}

export default NoConnectionPlaceholder;
