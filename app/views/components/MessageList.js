import React, { Component } from 'react';
import { ListView, Text, View, Keyboard, Animated } from 'react-native';
import styles from '../styles/styles';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import Message from './Message';
import NoMessagePlaceholder from './NoMessagePlaceholder';
import NoConnectionPlaceholder from './NoConnectionPlaceholder';
import constants from '../../constants';
import LoadingPage from './LoadingPage';

class MessageList extends Component {

  /*
    Here, we have the following props:
    {
      messages,
      appActions,
      deviceState,
      navigator,
      clientToken,
      resourceMap,
      connectionInfo,
    }
  */

  static propTypes = {}

  static defaultProps = {
    messagesDataSource: []
  };

  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this._onEndReached = this._onEndReached.bind(this);
    this.state = {
      messagesDataSource: ds.cloneWithRows(props.messages.messageList),
    };
  }


  componentDidUpdate(prevProps, prevState){
    let prevScrollToBottomState = prevProps.messages.shouldScrollToBottom;
    let newScrollToBottomState = this.props.messages.shouldScrollToBottom;
    let disableScrollToBottom = this.props.appActions.disableScrollToBottom;

    if(prevScrollToBottomState === false && newScrollToBottomState == true){
      if(this.refs.messagesListView){
        this.refs.messagesListView.scrollTo(0);
        // reset the shouldScrollToBottom flag state.
        disableScrollToBottom();  
      }
    }
  }


  render() {
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    let messageList = this.props.messages.messageList;
    let loadingState = this.props.messages.loadingState;
    let connectionInfo = this.props.connectionInfo;

    if(   loadingState === constants.messageLoadingStates.fresh
      ||  loadingState === constants.messageLoadingStates.loading
      ||  connectionInfo.subscriptionStatus !== constants.subscriptionStates.SUBSCRIBED){
        return <LoadingPage />;
    }
    if((loadingState === constants.messageLoadingStates.loaded) && (messageList.length === 0)){
      return <NoMessagePlaceholder />;
    }
    else if(loadingState === constants.messageLoadingStates.failed){
      return <NoConnectionPlaceholder
                fetchLastMessages={this.props.appActions.fetchLastMessages}
                clientToken={this.props.clientToken}
                messageMap={this.props.resourceMap.messages} />;
    }
    else{
      return (
        <ListView style={styles.messageList}
          ref='messagesListView'
          renderScrollComponent={props => <InvertibleScrollView {...props} inverted/>}
          dataSource={ds.cloneWithRows(messageList)}
          enableEmptySections={true}
          contentInset={{top: 0, bottom: this.state.keyboardOffset}}
          renderRow={message => this._renderMessage(message)}
          onEndReached={this._onEndReached}/>
      );
    }
  }

  _onEndReached(){
    let loadMoreMessages = this.props.appActions.loadMoreMessages;
    let markMessagesFinished = this.props.appActions.markMessagesFinished;
    let waitingForMoreStatus = this.props.messages.waitingForMoreStatus;
    let startPointer = this.props.messages.startPointer;
    let clientToken = this.props.clientToken;
    let messageMap = this.props.resourceMap.messages;


    let dir = 'down';

    if((waitingForMoreStatus===constants.loadingMoreMessagesStates.no) && (startPointer > 1)){
      let numberOfMessagesBefore = (startPointer-1);
      let total = Math.min(constants.maxMessageQueryNum, numberOfMessagesBefore);
      let options = {
        pointer: numberOfMessagesBefore,
        dir,
        total
      };

      loadMoreMessages(options, clientToken, messageMap);
    }
  }

  _renderMessage(someMessage) {
    if(someMessage){
      let theMessage = this.props.resourceMap.messages[someMessage];
      return(
        <View>
          <Message
            message={theMessage}
            messageKey={someMessage}
            photoMap={this.props.resourceMap.photos}
            deviceState={this.props.deviceState}
            appActions={this.props.appActions}
            navigator={this.props.navigator} />
          <Text>{someMessage}</Text>
        </View>
      );
    }
    else{
      return null;
    }

  }

}

export default MessageList;
