'use strict'
import React, { Component } from 'react';
import { Image, Animated, Text, View } from 'react-native';
import allStyles from '../styles/allStyles';
import * as utils from '../../utils/utils';
import constants from '../../constants';
import UploadingImage from './UploadingImage';


class MessagePhoto extends Component{
  /*
    Here, we will have the following props:
    {
      photo:{
        local_uri,
        uri,
        total_size,
      },
      photoMap,
      appActions,
      deviceState,
    }

    and the following state:
    {
      opacity,
      thumbnailImage,
      actualImage,
      localUri,
    }
  */

  constructor(props){
    //console.log('messagePhoto constructor called.');
    super(props);

    this._updatePhotoIfNeeded = this._updatePhotoIfNeeded.bind(this);
    this.state = {};



    this._updatePhotoIfNeeded();


    /*
    this.state = {
      opacity: new Animated.Value(1),
    };
    */
  }

  _updatePhotoIfNeeded(){

    let photo = this.props.photo || {};
    let photoMap = this.props.photoMap;
    let photoKey = photo.uri || photo.local_uri;

    let addPhoto = this.props.appActions.addPhoto;
    let loadImage = this.props.appActions.loadImage;

    if(photoKey){
      let thePhoto = photoMap[photoKey];
      if(!photoMap[photoKey] && !photo.download_status){
        /*
          just add the photo to photoKey.
        */

        addPhoto(photo, photoMap);
      }
      else{
        if(thePhoto.download_status === constants.downloadImageStates.WAITING){
          loadImage(photoMap[photoKey], photoMap);
        }

      }
    }
    else{
      //console.log('key does not exist.');
    }
  }


  /*
    This method is called when the component is receiving new props.
    We should check to see whether or not an update is required.
    If yes, we will initiate an image load.
  */
  componentDidUpdate(prevProps, prevState){
    this._updatePhotoIfNeeded();
  }


  render(){
    //debugger;

    let imageStyle = allStyles.message.messagePhoto;
    let imageWidth = (3/4 *this.props.deviceState.width) || constants.viewParams.defaultImageWidth;
    let imageHeight = imageWidth/constants.viewParams.imageAspectRatio;

    let photo = this.props.photo || {};
    let photoMap = this.props.photoMap;
    let photoKey = photo.uri || photo.local_uri;

    if(photoKey){
      if(photoMap[photoKey]){
        /*
          We have the photo in the photoMap.
          This means that a process is going on to update image info.

          The image info (available in photoMap[photoKey]) will contain the
          following info:
          {
            uri,
            local_uri,
            actualImage,
            thumbnailImage,
            total_size,
            uploaded_size,
            downloaded_size,
            download_status,
            upload_status,
            upload_request,
            download_request,
            error,
          }
          We should show the appropriate image based on the state of the photo.
        */
        let photoInfo = photoMap[photoKey];
        if(photoInfo.uri){
          /*
            we have a valid global link to the image. we should show it to the user.
            four states are possible:
              1. the image has a valid local_uri:
                  In this case, we should show the local_uri.
              2. the image is downloaded and can be shown:
                  In this case, we should render the image.
              3. the image is not downloaded, but a thumbnail for the image is
                  is downloaded and is available in this.state.thumbnailImage.
                  In this case, we should render the thumbnail.
              4. Neither the thumbnail nor the image is downloaded.
                  In this case we should render a placeholder.
          */

          if(photoMap[photoInfo.local_uri]){
            return (
              <Image
                style={imageStyle}
                source={{
                  uri: photoMap[photoInfo.local_uri].local_uri,
                  width: imageWidth,
                  height: imageHeight,
                }}
                resizeMode='cover' />
            );
          }
          else if(photoInfo.actual_image){
            return(
              <Image
                style={imageStyle}
                source={{
                  uri: photoInfo.actual_image,
                  width: imageWidth,
                  height: imageHeight,
                }}
                resizeMode='cover' />
            );
          }
          else if(photoInfo.thumbnail_image){
            return(
              <Image
                style={imageStyle}
                source={{
                  uri: photoInfo.thumbnail_image,
                  width: imageWidth,
                  height: imageHeight,
                }}
                resizeMode='cover' />
            );
          }
          else{
            return(
              <View style={[allStyles.message.imagePlaceholder, {width: imageWidth, height: imageHeight}]} />
            );
          }

        }
        else if(photoInfo.local_uri){
          /*
            We have a local photo that should be uploaded to the server.
            Here, we will have the image link in photoInfo.local_uri.
            The volume uploaded will be available via photoInfo.uploaded_size.
            Total volume of the image will be available via photoInfo.total_size.
            The image upload request will be available via photoInfo.upload_request.
            We may use it to let the user cancel the upload.
          */
          let uploadedSize = photoInfo.uploaded_size || 0;
          let totalSize = photoInfo.total_size || 10;
          let progress = Math.round(1000 * uploadedSize / totalSize)/10;

          return(
            <UploadingImage
              source={{
                uri: photoInfo.local_uri,
                width: imageWidth,
                height: imageHeight,
              }}
              progress={progress}
              uploadStatus={photoInfo.upload_status}
              uploadRequest={photoInfo.upload_request} />
          );
        }
        else{
          return null;
        }

      }
      else{
        /*
          The image is new and does not exist in the photoMap.
          We should add it to the photoMap.
          Until then
          , let's show a placeholder.
        */
        return (
          <View style={[allStyles.message.imagePlaceholder, {width: imageWidth, height: imageHeight}]} />
        );
      }
    }
    else{
      return null;
    }

  }

}

export default MessagePhoto;
