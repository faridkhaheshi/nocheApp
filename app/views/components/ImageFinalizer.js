import React, { Component } from 'react';
import { Image, Text, View, TouchableHighlight } from 'react-native';
import styles from '../styles/styles';
import Icon from 'react-native-vector-icons/FontAwesome';

/*
  This Comonents shows the selected image to user in order to finalize it.
  The user can either finalize it or order for a retry.

  We have the following props:
  {
    width,
    height,
    controllerHeight,
    isPortrait,
    source,
    onConfirm,
    onRetry,
    onClose,
  }
*/

class ImageFinalizer extends Component{

  constructor(props){
    super(props);

    this._onConfirm = this._onConfirm.bind(this);
    this._onRetry = this._onRetry.bind(this);
    this._onClose = this._onClose.bind(this);
  }

  _onConfirm(){
    this.props.onConfirm();
  }

  _onRetry(){
    this.props.onRetry();
  }

  _onClose(){
    this.props.onClose();
  }

  render(){

    let imageFinalizerContainerStyle = this.props.isPortrait ? (styles.imageFinalizedContainerPortrait) : (styles.imageFinalizedContainerLandscape);
    let imageFinalizerControllerStyle = this.props.isPortrait ? (
      [styles.imageFinalizerControllerPortrait, {height: this.props.controllerHeight, width: '100%'}]
      ) : (
      [styles.imageFinalizerControllerLandscape, {width: this.props.controllerHeight, height: '100%'}]
      );

    return(
      <View style={imageFinalizerContainerStyle}>
        <Image
          style={{height: this.props.height, width: this.props.width}}
          source={{uri: this.props.source.uri}} />

        <View
          ref='imageFinalizerController'
          style={imageFinalizerControllerStyle}
          >

          <View style={styles.closeImageFinalizerIconContainer}>
            <TouchableHighlight
              underlayColor='transparent'
              onPress={this._onClose}>
              <Icon name="close" size={30} style={styles.closeImageFinalizerIcon}/>
            </TouchableHighlight>
          </View>

          <View style={styles.confirmImageIconContainer}>
            <TouchableHighlight
              underlayColor='transparent'
              onPress={this._onConfirm}>
              <Icon name="check-circle" size={80} style={styles.confirmImageIcon}/>
            </TouchableHighlight>
          </View>

          <View style={styles.retryPictureIconContainer}>
            <TouchableHighlight
              underlayColor='transparent'
              onPress={this._onRetry}>
              <Icon name="refresh" size={30} style={styles.retryPictureIcon}/>
            </TouchableHighlight>
          </View>

        </View>

      </View>
    );
  }
}

export default ImageFinalizer;
