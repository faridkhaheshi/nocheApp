import React, { Component, } from 'react';
import { View, Text } from 'react-native';
import styles from '../styles/styles';
import constants from '../../constants';

class DialogBox extends Component {

  render() {
    let backgroundColor = this._getStateColor(this.props.alertState.colorState);
    if(this.props.alertState.shown){
      return (
        <View style={[styles.dialogBox, {backgroundColor: backgroundColor}]}>
          <Text style={styles.dialogBoxText}>{this.props.alertState.text}</Text>
        </View>
      );
    }
    else{
      return null;
    }
  }

  _getStateColor(colorState){
    let colorMap = constants.alertBoxColorMap;
    return colorMap[colorState];
  }
}

export default DialogBox;
