import constants from '../constants.js';
import * as apiServices from '../api/apiServices';
import { delay } from '../utils/utils';
import * as utils from '../utils/utils';
import fetchBlob from 'react-native-fetch-blob';


/*
function shareMessageViaTelegram(someMessage){

}
*/


function disableScrollToBottom(){
  return({
    type: constants.actionTypes.CHANGE_SCROLL_TO_BOTTOM_STATE,
    payload:{
      changeTo: false,
    },
  });
}

/*
  This action creator is used to select a message.
  It dispatches a SELECT_MESSAGE action.
  a menu is called when a message is selected.
*/
function selectMessage(someMessageKey){
  return({
    type: constants.actionTypes.SELECT_MESSAGE,
    payload:{
      messageKey: someMessageKey,
    },
  });
}



/*
  This method is responsible to update deviceState.
  The input object will include the following fields:
  {
    width: newWidth
    height: newHeight
    isPortrait: newOriantation
  }
*/
function setDeviceState(newDeviceState){
  return({
    type: constants.actionTypes.SET_DEVICE_STATE,
    payload: newDeviceState,
  });
}



function togglePhotoModalVisibility(){
  return {
    type: constants.actionTypes.TOGGLE_PHOTO_MODAL_VISIBILITY,
    payload: {}
  };
}

/*
  This method loads more messages.
  options include:
  {
    pointer,
    dir,
    total
  }

  It also changes the waitingForMore flag to true in order to avoid
  repetition of the same request.

*/
function loadMoreMessages(options, clientToken, messageMap){
  return function(dispatch){
    dispatch({
      type: constants.actionTypes.MARK_LOADING_MORE_MESSAGES_STATUS,
      payload: {
        status: constants.loadingMoreMessagesStates.waiting
      }
    });

    apiServices.loadMoreMessages(options, clientToken)
      .then((serverResponse) => {
        if(serverResponse.ok){
          let messages = serverResponse.data;

          dispatch({
            type: constants.actionTypes.ADD_MORE_MESSAGES,
            payload: {
              messages,
              options,
              messageMap,
            },
          });
        }
        else if((serverResponse || {}).status){
          // got an error from server.
          let theAction = interpretServerResponseStatus(serverResponse.status,
                                    {tokenType: constants.tokenTypes.access});
          dispatch(theAction);
        }
        else{
          dispatch({
            type: constants.actionTypes.MARK_LOADING_MORE_MESSAGES_STATUS,
            payload: {
              status: constants.loadingMoreMessagesStates.failed
            },
            error: 'server response does not include data'
          });
        }
      })
      .catch((error) => {

        dispatch({
          type: constants.actionTypes.MARK_LOADING_MORE_MESSAGES_STATUS,
          payload: {
            status: constants.loadingMoreMessagesStates.failed
          },
          error
        });
      });
  };
}

/*
  This method fetches the user's last messages from server.
  It is called when the app is opened for the first time.
*/
function fetchLastMessages(clientToken, messageMap){
  return function(dispatch){
    dispatch({
      type: constants.actionTypes.SET_LOADING_STATE,
      payload: {
        state: constants.messageLoadingStates.loading,
      },
    });

    apiServices.fetchLastMessages(clientToken)
      .then((serverResponse) => {
        if((serverResponse || {}).ok){
          // got a valid response from server.
          dispatch({
            type: constants.actionTypes.LOAD_MESSAGES,
            payload: {
              messages: serverResponse.data,
              messageMap,
            },
          });
        }
        else if((serverResponse || {}).status){
          // got an error from server.
          let theAction = interpretServerResponseStatus(serverResponse.status);
          dispatch(theAction);
        }
        else{
          dispatch({
            type: constants.actionTypes.SET_LOADING_STATE,
            payload: {
              state: constants.messageLoadingStates.failed,
            },
            error,
          });
        }
      })
      .catch((error) => {
        console.log('Error happened when fetching messages.');
        console.log(error);
        dispatch({
          type: constants.actionTypes.SET_LOADING_STATE,
          payload: {
            state: constants.messageLoadingStates.failed,
          },
          error,
        });
      });
  };
}

/*
  This method updates the app state based on the status code received from
  APIs.

  401 status code means that the token is invalid.

  in case of a 401 error, the otherData input shoud include the tokenType as:

  {
    tokenType: 'access' | 'refresh'
  }

*/
function interpretServerResponseStatus(statusCode){
  if(statusCode === 401){
    return({
      type: constants.actionTypes.MARK_TOKEN_VALIDITY,
      payload:{
        isValid: false,
      },
    });
  }
  else{
    return({
      type: constants.actionTypes.NOTHING,
      payload: {},
    });
  }
}

/*
  This method is responsible for showing alert in the alert bar.

*/
function showAlert(alertText, colorState = 'green'){
  return {
    type: constants.actionTypes.SHOW_ALERT,
    payload: {
      text: alertText,
      colorState
    }
  };
}

/*
  This method is responsible for hiding alert bar.
*/
function hideAlert(){
  return {
    type: constants.actionTypes.HIDE_ALERT,
    payload: {}
  };
}


/*
  This method handles the loading of images.
  It first loads a thumbnail of the image and saves it to the photoMap in state.messages.
  Then it loads the actual image and saves it to the photoMap.

  photo will include the following info:
  {
    local_uri,
    uri,
    actual_image,
    thumbnail_image,
    total_size,
    uploaded_size,
    downloaded_size,
    download_status,
    upload_status,
    upload_request,
    download_request,
    error,
  }

*/
function loadImage(somePhoto, photoMap){
  return function(dispatch){
    let photo = somePhoto || {};
    if(photo.download_status === constants.downloadImageStates.WAITING){
      // first we change the photo download status to DOWNLOADING.
      let photoKey = photo.uri || photo.local_uri;
      let imageURL = somePhoto.uri;

      if(imageURL){
        dispatch({
          type: constants.actionTypes.UPDATE_PHOTO_DOWNLOAD_STATUS,
          payload:{
            photoKey,
            status: constants.downloadImageStates.DOWNLOADING,
            photoMap,
          },
        });

        let thumbnailURL = utils.convertImageURL(imageURL, constants.imageContexts.messageThumbnail);
        let messagePhotoURL = utils.convertImageURL(imageURL, constants.imageContexts.messagePhoto);

        //loading the image thumbnail:
        fetchBlob.fetch('GET', thumbnailURL, {
            //Authorization : 'Bearer access-token...',
          })
          .then((imageData) => {
            let base64Str = imageData.data;
            let imageBase64 = 'data:' + 'image/jpeg' +';base64,'+base64Str;

            photo.thumbnail_image = imageBase64;

            dispatch({
              type: constants.actionTypes.UPDATE_PHOTO,
              payload:{
                photo,
                photoKey,
                photoMap,
              },
            });
          })
          .catch((errorMessage, statusCode) => {
            photo.error = {
              description: errorMessage,
              error_code: statusCode,
            };

            dispatch({
              type: constants.actionTypes.UPDATE_PHOTO,
              payload:{
                photo,
                photoKey,
                photoMap,
              },
              error: photo.error,
            });
          });

        //loading the actual image:
        fetchBlob.fetch('GET', messagePhotoURL, {
            //Authorization : 'Bearer access-token...',
          })
          .then((imageData) => {
            let base64Str = imageData.data;
            let imageBase64 = 'data:' + 'image/jpeg' +';base64,'+base64Str;

            photo.actual_image = imageBase64;
            photo.download_status = constants.downloadImageStates.DOWNLOADED;

            dispatch({
              type: constants.actionTypes.UPDATE_PHOTO,
              payload:{
                photo,
                photoKey,
                photoMap,
              },
            });

          })
          .catch((errorMessage, statusCode) => {
            photo.error = {
              description: errorMessage,
              error_code: statusCode,
            };

            dispatch({
              type: constants.actionTypes.UPDATE_PHOTO,
              payload:{
                photo,
                photoKey,
                photoMap,
              },
              error: photo.error,
            });
          });

      }
      else{
        //TODO: handle this case later. we don't have a uri for the photo.
      }


    }

  };
}

/*
  This method adds a new image to the photoMap.
  photo will include the following info:
  {
    uri,
    local_uri,
    total_size,
  }
*/
function addPhoto(photo, photoMap){
  return ({
    type: constants.actionTypes.ADD_PHOTO,
    payload: { photo, photoMap },
  });
}

/*
  This function adds a new message to Noche server.
  Before the new message is received by the server, it will be shown in the message list as
  'ON_DEVICE' message.
  When the message is received by the server, its status will change to 'ON_SERVER'.
  The input argument is a message in this format:
  {
    text: 'someText'
  }
*/
function sendMessage(someMessage, token, nextDraftIndex, resourceMap){

  //debugger;

  /*
    This is a thunk-based actionCreator.
    We first dispatch an action in which the message status is 'ON_DEVICE',
    then, when the message is received by the server, we dispatch another event with the
    message with 'ON_SERVER' status.
    If the send process fails, the message will have a 'SEND_FAILED' status.
    Here, we add the timeStamp to mark the message on device.
    When the result is received from the server, we use this timestamp to identify the
    message that must be updated.
  */
  return function(dispatch){

    /*
      If the message is a photo, we should first upload the photo to our cdn.
      The message state will be PREPARING first until the photo is fully uploaded
      and a valid global url is obtained to access it over internet.
      Then, the message status will be changed to ON_DEVICE and the process will go on
      as usual:
      The message will be sent to the server.
      The server will give the message a valid message_index and return it to client.

    */


    let theMessage = someMessage;
    let draftIndex = nextDraftIndex || 0;
    let messageMap = resourceMap.messages;
    let photoMap = resourceMap.photos;

    theMessage.state = constants.messageStates.WAITING_TO_BE_INDEXED;
    theMessage.sender = constants.messageSenders.USER;

    let prepareMessagePromise = Promise.resolve(null);

    if(someMessage.photo){

      theMessage.state = constants.messageStates.BEING_PREPARED;
      let photo = someMessage.photo;
      let photoKey = photo.uri || photo.local_uri;


      dispatch({
        type: constants.actionTypes.ADD_PHOTO,
        payload:{ photo, photoMap },
      });

      prepareMessagePromise = apiServices.uploadPhoto(photo.local_uri, (error, update) => {
        /*
          here, we will have the status for the upload progress.
          The update will have the following form:
          {
            status: 'uploading' | 'uploaded'
            progress: {
              uploaded: //number of bytes uploaded,
              total: //number of bytes to be uploaded,
            },
          }

          if an error happens for any reason, the error will include more info about the error:
          {
            description: '',
            error_code: // one of constats.serverErrors
          }
        */



        if(error){
          // handle the error
          photo.upload_status = constants.uploadImageStates.FAILED;
          dispatch({
            type: constants.actionTypes.UPDATE_PHOTO,
            payload: {
              photo,
              photoKey,
              photoMap,
            },
            error,
          });
        }
        else{
          // update the state of the photo that must be uploaded.

          if(update.status === constants.uploadImageStates.UPLOADING){
            photo.total_size = update.progress.total;
            photo.uploaded_size = update.progress.uploaded;
            photo.upload_request = update.progress.uploadRequest;
            photo.upload_status = constants.uploadImageStates.UPLOADING;
          }
          else if(update.status === constants.uploadImageStates.UPLOADED){
            photo.upload_status = constants.uploadImageStates.UPLOADED;
            photo.uploaded_size = update.totalSize;
            photo.total_size = update.totalSize;
            photo.uri = update.uri;
          }

          dispatch({
            type: constants.actionTypes.UPDATE_PHOTO,
            payload: {
              photo,
              photoKey,
              photoMap,
            },
          });

        }

      });

    }

    dispatch({
      type: constants.actionTypes.ADD_MESSAGE,
      payload: {
        message: theMessage,
        draftIndex,
        messageMap,
      }
    });

    dispatch({
      type: constants.actionTypes.CHANGE_SCROLL_TO_BOTTOM_STATE,
      payload:{
        changeTo: true,
      },
    });

    prepareMessagePromise
      .then((photoInfo) => {
        if(photoInfo){
          // we had some photo in the message and it has been uploaded to a server.
          /*
            photoInfo includes:
            {
              uri,
              totalSize,
              localURI,
            }
          */
          theMessage.photo.uri = photoInfo.uri;
          theMessage.photo.total_size = photoInfo.totalSize;
          theMessage.state = constants.messageStates.WAITING_TO_BE_INDEXED;


          delete theMessage.photo.upload_request;
          delete theMessage.photo.uploaded_size;
        }

        return apiServices.sendMessage(theMessage, token);
      })
      .then((result) => {
        if(result.ok){
          let serverMessage = (result.data || {}).message;
          dispatch({
            type: constants.actionTypes.UPDATE_MESSAGE,
            payload: {
              message: serverMessage,
              draftIndex,
              messageMap,
            }
          });
        }
        else if((result || {}).status){
          // got an error from server.
          let theAction = interpretServerResponseStatus(result.status,
                                    {tokenType: constants.tokenTypes.access});
          dispatch(theAction);
        }
        else{
          // Something wrong in the process but we received a result from the server.
          theMessage.state = constants.messageStates.FAILED_TO_INDEX;
          dispatch({
            type: constants.actionTypes.UPDATE_MESSAGE,
            payload: {
              message: theMessage,
              draftIndex,
              messageMap,
            }
          });

          // TODO: extra message. e.g. information about the reason for the failure.
        }
      })
      .catch((error) => {
        // Something went wrong in the process. maybe network error or server down.
        theMessage.state = constants.messageStates.FAILED_TO_INDEX;
        dispatch({
          type: constants.actionTypes.UPDATE_MESSAGE,
          payload: {
            message: theMessage,
            draftIndex,
            messageMap,
          },
          error: true
        });

        // TODO: extra message. for example show the user that there may be a problem in internet connection etc.
      });
  }
}

/*
  This method is called when a certain callback data should be sent to the server.
  It is done in async.
*/
function sendCallbackData(inlineKey){
  return function(dispatch){
    inlineKey.status = constants.buttonStates.WAITING;
    dispatch({
      type: constants.actionTypes.SEND_CALLBACK_DATA,
      payload: { ...inlineKey }
    });

    apiServices.sendCallbackData(inlineKey.callback_data)
      .then((result) => {
        inlineKey.status = constants.buttonStates.ACTIVE;
        dispatch({
          type: constants.actionTypes.SEND_CALLBACK_DATA,
          payload: { ...inlineKey }
        });
      })
      .catch((error) => {
        inlineKey.status = constants.buttonStates.FAILED;
        dispatch({
          type: constants.actionTypes.SEND_CALLBACK_DATA,
          payload: { ...inlineKey }
        });
      });
  }
}


/*
  This method is responsible to establish server connections.
  When this is done, the server will be able to send messages and data to client
  over the established channels.
  It is async
*/
function establishServerConnections(serverConnection, connectionInfo, resourceMap){
  return function(dispatch){

    let clientToken = connectionInfo.clientToken;
    let channels = connectionInfo.channels;
    let pusher = serverConnection.connect({ clientToken });

    pusher.connection.bind('connected', () => {
      dispatch({
        type: constants.actionTypes.MARK_CONNECTION_STATE,
        payload: {
          serverConnection,
          state: constants.connectionStates.CONNECTED,
        },
      });
    });

    pusher.connection.bind('connecting', () => {
      dispatch({
        type: constants.actionTypes.MARK_CONNECTION_STATE,
        payload: {
          serverConnection,
          state: constants.connectionStates.CONNECTING,
        },
      });
    });

    pusher.connection.bind('unavailable', () => {
      dispatch({
        type: constants.actionTypes.MARK_CONNECTION_STATE,
        payload: {
          serverConnection,
          state: constants.connectionStates.UNAVAILABLE,
        },
      });
    });

    pusher.connection.bind('failed', () => {
      dispatch({
        type: constants.actionTypes.MARK_CONNECTION_STATE,
        payload: {
          serverConnection,
          state: constants.connectionStates.FAILED,
        },
      });
    });

    pusher.connection.bind('disconnected', () => {
      dispatch({
        type: constants.actionTypes.MARK_CONNECTION_STATE,
        payload: {
          serverConnection,
          state: constants.connectionStates.DISCONNECTED,
        },
      });
    });

    // Change subscriptionStatus to SUBSCRIBING
    dispatch({
      type: constants.actionTypes.MARK_SUBSCRIPTION_STATE,
      payload: {
        serverConnection,
        state: constants.subscriptionStates.SUBSCRIBING,
        channels
      }
    });

    channels.map((channelName) => {
      let theChannel = serverConnection.subscribeTo({ channelName });
      theChannel.bind('pusher:subscription_succeeded', () => {
        dispatch({
          type: constants.actionTypes.MARK_SUBSCRIPTION_STATE,
          payload: {
            serverConnection,
            state: constants.subscriptionStates.SUBSCRIBED,
            channel: channelName,
            channels
           }
        });
      });

      theChannel.bind('pusher:subscription_error', (status) => {
        dispatch({
          type: constants.actionTypes.MARK_SUBSCRIPTION_STATE,
          payload: {
            serverConnection,
            state: constants.subscriptionStates.FAILED,
            channel: channelName,
            channels
          },
          error: status
        });
      });

      /*
        let's define the callback function to handle the messages received over this channel.
        We will dispatch a new message action when the message is received.
       */
      theChannel.bind('new_message', (data)=> {

        let theMessage = data.message;
        let messageMap = resourceMap.messages;

        dispatch({
          type: constants.actionTypes.ADD_MORE_MESSAGES,
          payload: {
            messages: [ theMessage ],
            options: { dir: 'up' },
            messageMap,
          }
        });
      });


    });
  };
}

/*
  This method sets the clientInfo to app state.
  clientInfo includes:
  {
    token,
    channels,
  }
*/
function setClientInfo(clientInfo){
  return function(dispatch){
    setTimeout(
      dispatch({
        type: constants.actionTypes.SET_CLIENT_INFO,
        payload: { clientInfo }
      }), constants.appParams.loadTime);
  };
}

/*
  This method initializes the Client.
  Initialization means fetching a working client token.
  It checks for the token in the safe info storage.
  If the token exists in the storage, the function initiates the opening of the app.
  If no token exists in device, the function sends the device info to the server and asks for
  a valid token.
*/
function initializeClient(){
  return function(dispatch){
    let token;

    apiServices.fetchClientInfo()
      .then( (clientInfo) => {

        if((clientInfo || {}).status){
          let theAction = interpretServerResponseStatus(clientInfo.status);
          dispatch(theAction);
        }
        else{
          dispatch({
            type: constants.actionTypes.SET_CLIENT_INFO,
            payload: { clientInfo }
          });
        }
      })
      .catch((error) => {
        console.log('error happened. retrying');
        console.log(error);
        dispatch({
          type: constants.actionTypes.RETRY_INITIALIZATION,
          payload: {},
          error
        });
      });
  };
}


/*
  This method renews the access token.
  It does the following actions:
    1. uses the refreshToken to get a valid access token.
    2. (if 1 does not work) registers the device and gets completely new token pair.
*/
function solveTokenValidityProblem(){
  return function(dispatch){

    apiServices.solveTokenValidityProblem()
      .then((solutionResult) => {
        if((solutionResult || {}).done){
          dispatch({
            type: constants.actionTypes.SET_CLIENT_INFO,
            payload: {
              clientInfo: solutionResult.clientInfo,
            },
          });
        }
        else{
          // the situation remains the same. retry is possible.
          dispatch({
            type: constants.actionTypes.RETRY_INITIALIZATION,
            payload: {},
            error: 'solution unsuccessful',
          });
        }
      })
      .catch((error) => {
        console.log('error happened:');
        console.log(error);
        // retry is possible.
        dispatch({
          type: constants.actionTypes.RETRY_INITIALIZATION,
          payload: {},
          error,
        });
      });

  };
}



export default {
  disableScrollToBottom,
  sendMessage,
  selectMessage,
  sendCallbackData,
  setClientInfo,
  initializeClient,
  establishServerConnections,
  showAlert,
  hideAlert,
  fetchLastMessages,
  loadMoreMessages,
  togglePhotoModalVisibility,
  setDeviceState,
  loadImage,
  addPhoto,
  solveTokenValidityProblem,
};
