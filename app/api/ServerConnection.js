import pusher from 'pusher-js/react-native';
import constants from '../constants.js';

// Enable pusher logging - don't include this in production
pusher.logToConsole = true;
  /*
    ServerConnection is the connection that provides server initiated events to be accessible
    on device.

    The general use case:

    let serverConnection = new ServerConnection({
      appKey: 'some pusher app key',
      cluster: 'eu',
      isEncrypted: true,
      authEndpoint: '/pusher/auth'
    });

    serverConnection
      .connect({clientToken: 'someToken'})
      .subscribeTo({channelName: 'private-channel-name'});


    serverConnection will have the following form:
    {
      pusherInfo:{},
      pusher: {},
      channels: {},
    }


  */

export default class ServerConnection{

  constructor(props){
    this.pusherInfo = {
      appKey: props.appKey,
      cluster: props.cluster,
      isEncrypted: props.isEncrypted,
      authEndpoint: props.authEndpoint
    };
    this.channels = {};
  }

  /*
    To start the ServerConnection, you should provide the requiredData like this.
    serverConnection.connect({clientToken: 'someToken'});

    It will add a pusher object to the ServerConnection and return the updated
    ServerConnection.
  */
  connect(requiredData){
    if((requiredData || {}).clientToken){
      let authParams = {};
      authParams[constants.clientTokenKey] = requiredData.clientToken;

      this.pusher = new pusher(this.pusherInfo.appKey, {
        cluster: this.pusherInfo.cluster,
        encrypted: this.pusherInfo.isEncrypted || true,
        authEndpoint: this.pusherInfo.authEndpoint || '/pusher/auth',
        auth: { params: authParams },
      });
    }

    return this.pusher;
  }


  /*
    After the connection is established, this method is used to subscribe to
    an specific channel.
    serverConnection.subscribeTo({channelName: 'private-channel-name'});
    calling this method will subscribe the pusher to the specified channel.
    This channel will be accessible in this.channel.
    This method returns the channel object created.
  */
  subscribeTo(connectionInfo){
    if(this.pusher){
      this.channels[connectionInfo.channelName] = this.pusher.subscribe(connectionInfo.channelName);
      return this.channels[connectionInfo.channelName];
    }
    else{
      return null;
    }
  }


}
