import constants from '../constants';
import SInfo from 'react-native-sensitive-info';
import * as utils from '../utils/utils';
import CryptoJS from 'crypto-js';


//const APIroot = 'https://nocheserver.herokuapp.com/api';
const apiRoot = constants.baseServerURL;
const authAPI = apiRoot + '/auth';
const messageAPI = apiRoot + '/api/message';

let API = {
  auth: authAPI,
  message: messageAPI
};



/*
  This method uploads the given photo to cdn and gives back the global uri to access it.
  a callback function will be given to this method. This function will be used as follows:

    - when the upload is in progress, the callback function will be called with some information about the
      progress of the uploading:
      cb(null, {
        status: 'uploading',
        progress: {
          uploaded: //number of bytes uploaded
          total: //total number of bytes to be uploaded
        },
      })
    - when the upload is finished, the call back function will be called with the following data:
      cb(null, {
        status: 'uploaded',
        uri: 'the uri for the image.',
      });
    - if an error happens in the upload process, the call back function will be called:
      cb({
        description: 'error description',
        error_code: //one of constats.serverErrors
      }, null);

*/
export function uploadPhoto(localURI, cb){

  return new Promise((resolve, reject) =>{
    let timestamp = (Date.now()/1000 | 0);
    let api_key = '326279958474146';
    let api_secret = 'wsEb4fvFdMhDva6HA3FsCTLKZvE';
    let uploudURL = 'https://api.cloudinary.com/v1_1/nimvajabicloud/image/upload';
    let hashString = 'timestamp=' + timestamp + api_secret;
    //TODO: we should send the data to server and get the signature from server. The api secret should not be available on client.
    let signature = CryptoJS.SHA1(hashString).toString();
    let xhr = new XMLHttpRequest();
    let totalSize;
    xhr.open('POST', uploudURL);
    xhr.responseType = 'json';


    /*
    xhr.onreadystatechange = () => {
      if(xhr.readyState === XMLHttpRequest.DONE){
        console.log('Image upload done.');
        console.log(xhr.response);
        console.log(xhr);
      }
    };
    */

    xhr.onerror = (e) => {
      // we have a problem in the network error.
      cb({
        description: `request status ${xhr.status}`,
        error_code: constants.serverErrors.UNSUCCESSFUL_RESPONSE,
      });
    };

    xhr.ontimeout = (e) => {
      cb({
        description: `request status ${xhr.status}`,
        error_code: constants.serverErrors.TIMEOUT,
      });
    };

    xhr.upload.onloadstart = (e) => {
      cb(null, {
        status: constants.uploadImageStates.UPLOADING,
        progress: {
          uploaded: 0,
          total: e.total,
          uploadRequest: xhr,
        },
      });
      totalSize = e.total;
    };

    xhr.upload.onprogress = (e) => {
      if(e.lengthComputable){
        cb(null, {
          status: constants.uploadImageStates.UPLOADING,
          progress: {
            uploaded: e.loaded,
            total: e.total,
            uploadRequest: xhr,
          },
        });

        totalSize = e.total;
      }
    };

    xhr.onload = (e) => {
      // The request is complete.
      let theResponse = xhr.response;

      if(xhr.status === 200){
        // request responsed successfully.
        if(theResponse.url){
          cb(null, {
            status: constants.uploadImageStates.UPLOADED,
            uri: theResponse.secure_url,
            totalSize
          });

          resolve({
            uri: theResponse.secure_url,
            totalSize,
            localURI,
          });
        }
        else{
          cb({
            description: 'The response does not include a valid url.',
            error_code: constants.serverErrors.UNSUCCESSFUL_RESPONSE,
          });
        }
      }
      else{
        // some error happened for some reason.
        cb({
          description: `received status ${xhr.status} from server.`,
          error_code: constants.serverErrors.UNSUCCESSFUL_RESPONSE,
        });
      }
    };

    // let's prepare the form data to be sent to upload server.
    let formData = new FormData();
    formData.append('file', {uri: localURI, type: 'image/jpg', name: '1.jpg'});
    formData.append('timestamp', timestamp);
    formData.append('api_key', api_key);
    formData.append('signature', signature);


    // let's send the data.

    xhr.send(formData);
  });
}






/*
  This method loads more messages from server based on the given options.
  The options include:
  {
    pointer,
    dir,
    total
  }
  It returns a promise resolving with the response received from server.

  The response received from server will include:
  {
    ok: 'ok',
    data: {},
  }
*/
export function loadMoreMessages(options, clientToken){
  return new Promise((resolve, reject) => {

    let queryStr = Object.keys(options)
                  .map(k => encodeURIComponent(k)+'='+encodeURIComponent(options[k]) )
                  .join('&');
    let fullApiPath = API.message + '?' + queryStr;

    fetch(fullApiPath, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'noche_client_token': clientToken
      },
    })
      .then((response) => {

        if(response.status === 200){
          // received a valid response from server.
          return response.json();
        }
        else{
          resolve({ status: response.status });
          return (null);
        }
      })
      .then((responseJson) =>{
        if(responseJson){
          resolve(responseJson);
        }
      })
      .catch((err) => {
        reject(err);
      });
  });
}

/*
  This method fetches the last messages from server.
  It returns a promise resolving with the list of messages received from the server.

*/
export function fetchLastMessages(clientToken){
  return new Promise((resolve, reject) => {

    fetch(API.message, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'noche_client_token': clientToken
      },
    })
      .then((response) => {

        if(response.status === 200){
          // received a valid response from server.
          return response.json();
        }
        else{
          resolve({ status: response.status });
          return (null);
        }
      })
      .then((responseJson) =>{
        if(responseJson){
          resolve(responseJson);
        }
      })
      .catch((err) => {
        console.log('error:');
        console.log(err);
        reject(err);
      });
  });
}

/*
  Here, we have all the services provided by the api.
  We add user info to the requests and send it to the server.
  It is server's responsibility to decide what to send back to be shown to the user.

*/


/*
  This method sends a message to the server.
  It return a promise.
  The promise resolves with an object in this form:
  {
    ok: 'ok',
    data: { message: theMessageObject },
    error: 'errorMessage',
  }
*/

export function sendMessage(someMessage, token){

  return new Promise((resolve, reject) => {
    fetch(API.message, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'noche_client_token': token
      },
      body: JSON.stringify(someMessage)
    })
      .then((response) => {
        if(response.status === 200){
          // received a valid response from server.
          return response.json();
        }
        else{
          resolve({ status: response.status });
          return (null);
        }
      })
      .then((responseJson) =>{
        if(responseJson){
          resolve(responseJson);
        }
      })
      .catch((err) => {
        reject(err);
      });

  });
}

export function sendCallbackData(callbackData){
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        ok: 'ok',
        data: { callback_data: callbackData }
      });
    }, 4000);
  });
}

/*
  This method fetches the local token from Safe Info Storage.
  It returns a promise resolving with the clientInfo.
  the clientInfo will include the following info:
  {
    token: {
            access_token: <JWT>,
            refresh_token: <JWT>,
            expiration_time: <integer time>       // for the access token
          },
    channels: [
      "private-user-18238123hbahsjbdhasbdjhabs",
      "private-device-58f596d417a80b26b84abf42"
    ]
  }


*/
function fetchLocalClientInfo(){
  return new Promise((resolve, reject) => {
    // let's resolve with some fake clientInfo:

    SInfo.getItem(constants.clientInfoKey, constants.sensitiveInfoOptions)
      .then((clientInfoStr) => {
        if(clientInfoStr){
          resolve(JSON.parse(clientInfoStr));
        }
        else{
          resolve(null);
        }
      })
      .catch((error) => {
        reject(error);
      });

  });
}

/*
  This method fetches client info for the client.
  client info includes a token pair and list of channels to subscribe to.

  It first checks in the local storage for saved clientInfo.
  If no clientInfo is stored on device, it will send device info for nocheServer and
  gets client info from server.
*/
export function fetchClientInfo(){
  return new Promise((resolve, reject) => {
    let theToken;
    fetchLocalClientInfo()
      .then((clientInfo) => {

        if(clientInfo){

          // let's check to see if the local token is still valid.
          let now = Math.floor((Date.now())/1000);
          let tokenExpirationTime = clientInfo.token.expiration_time;
          let tomorrow = now + 60*60*24;

          if(tokenExpirationTime < tomorrow){
            let refreshT = clientInfo.token.refresh_token;
            return refreshToken(refreshT);
          }
          else{
            resolve(clientInfo);
            return null;
          }
        }
        else{

          return registerDevice();
        }
      })
      .then((clientInfo) => {

        if((clientInfo || {}).ok){
          delete clientInfo.ok;
          saveClientInfoLocally(clientInfo);
          resolve(clientInfo);
        }
        else if((clientInfo || {}).status){
          resolve(clientInfo);
        }
        else if(clientInfo){
          reject(new Error('server response does not include client info.'));
        }
      })
      .catch((err) => {
        reject(err);
      });
  });
}

/*
  This method tries to solve the token validity problem.
  First, it tries to get a new access token using the refresh token available locally.
  Then, it will try to register the device again.

  This method returns a promise resolving with the result.
  The result has the following form:

  {
    done,
    clientInfo,
  }
*/
export function solveTokenValidityProblem(){
  return new Promise((resolve, reject) => {
    fetchLocalClientInfo()
      .then((clientInfo) => {
        if(clientInfo){
          let refreshT = clientInfo.token.refresh_token;
          return refreshToken(refreshT);
        }
        else{
          return null;
        }
      })
      .then((serverResponse) => {
        if((serverResponse || {}).ok){
          delete serverResponse.ok;
          let clientInfo = serverResponse;
          saveClientInfoLocally(clientInfo);
          resolve({
            done: true,
            clientInfo,
          });
          return null;
        }
        else if(serverResponse.status === 401){
          removeLocalClientInfo();
          return registerDevice();
        }
        else{
          return registerDevice();
        }
      })
      .then((serverResponse) => {
        if((serverResponse||{}).ok){
          delete serverResponse.ok;
          let clientInfo = serverResponse;
          saveClientInfoLocally(clientInfo);
          resolve({
            done: true,
            clientInfo,
          });
        }
        else{
          resolve(null);
        }
      })
      .catch((error) => {
        reject(error);
      })
  });
}

/*
  This method registers the device to the server.
  In response, the server sends a valid token pair and a list of channels
  to subscribe to.
  It returns a promise resolving with the clientInfo.
  {
    token: {
            access_token: <JWT>,
            refresh_token: <JWT>,
            expiration_time: <integer time>       // for the access token
          },
    channels: [
      "private-user-18238123hbahsjbdhasbdjhabs",
      "private-device-58f596d417a80b26b84abf42"
    ]
  }
*/
function registerDevice(){
  return new Promise((resolve, reject) => {
    let deviceInfo = utils.fetchDeviceInfo();
    fetch(API.auth, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ deviceInfo })
    })
      .then((response) => {
        if(response.status === 200){
          // received a valid response from server.
          return response.json();
        }
        else{
          resolve({ status: response.status });
          return (null);
        }
      })
      .then((responseJson) =>{
        if(responseJson){
          resolve(responseJson);
        }
      })
      .catch((err) => {
        reject(err);
      });
  });
}


/*
  This method asks for a new token pair from server.
  It sends the refresh token on the device to server.

  This method returns a promise resolving with the token pair.
*/
function refreshToken(refreshToken){
  return new Promise((resolve, reject) => {
    let deviceInfo = utils.fetchDeviceInfo();
    fetch(API.auth, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ refreshToken })
    })
      .then((response) => {
        if(response.status === 200){
          // received a valid response from server.
          return response.json();
        }
        else{
          resolve({ status: response.status });
          return (null);
        }
      })
      .then((responseJson) =>{
        if(responseJson){
          resolve(responseJson);
        }
      })
      .catch((err) => {
        reject(err);
      });
  });
}


/*
  This method saves the clientInfo retrieved from server to local storage.
  It does it synchronously.
  clientInfo includes:
  {
    token:
    channels:
  }
  It returns true when done.
*/
function saveClientInfoLocally(clientInfo){
  let clientInfoStr = JSON.stringify(clientInfo);
  SInfo.setItem(constants.clientInfoKey, clientInfoStr, constants.sensitiveInfoOptions);
  return true;
}


function removeLocalClientInfo(){
  SInfo.deleteItem(constants.clientInfoKey, constants.sensitiveInfoOptions);
}
