'use strict'
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import NocheApp from './NocheApp';
import store from '../store/store';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <NocheApp />
      </Provider>
    );
  }
}

export default App;
