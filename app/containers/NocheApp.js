'use strict';

import React, {Component} from 'react';
//import { Text, PixelRatio } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actionCreators from '../actions/actionCreators';
import AppNavigator from '../views/navigation/AppNavigator';
import ServerConnection from '../api/ServerConnection';
import constants from '../constants';

class NocheApp extends Component {
  /*
    This is the mail noche app.
    It has the state as a prop (this.props.appState);
    It also has all the action creators as props.

    All data regarding messages and photos used in the app will be saved here as
    this.resourceMap.
    It will include messages as this.resourceMap.messages and
    photos as this.resourceMap.photos.

    messageMap will include all messages data as a key-value store.
    each message will have the following info:
    {
      text                    :        String,
      sender                  :       { type: String, required: true },               // USER | BOT
      device_id               :       { type: Schema.Types.ObjectId, ref: 'Device' },
      user_id                 :       { type: Schema.Types.ObjectId, ref: 'User' },
      message_index           :       { type: Number, required: true },
      photo                   :       {
        local_uri             :       String,
        uri                   :       String,
        total_size            :       Number,
      },
      state                   :       { type: String, required: true },
      time_stamp              :       { type: Number, required: true },
      inline_keyboards        :       [ inlineKeyboardModel ]
    }
    The key to the messageMap will be message_index.

    photoMap will include all data about photos as a key-value store.
    each photo will have the following info about the photo:
    {
      local_uri,
      uri,
      actual_image,
      thumbnail_image,
      total_size,
      uploaded_size,
      downloaded_size,
      download_status,
      upload_status,
      upload_request,
      download_request,
      error,
    }

    the key to this store is the photoKey (uri || local_uri).

    NocheApp will also have a messageList.
    The messageList is the list of message indices that are currently being shown.
    each entry of this array is an index. either a number (e.g. 433) or a string
    (e.g. d10).


  */

  constructor(props) {
    super(props);

    this.serverConnection = new ServerConnection(constants.pusherAppInfo);
    this.resourceMap = {
      messages:{},
      photos:{},
    };
  }

  componentDidUpdate(prevProps, prevState){

    console.log('inside componentDidUpdate of NocheApp. appState:');
    let appState = this.props.appState;
    console.log(appState);

  }

  render() {
    return (
      <AppNavigator
        {...this.props}
        serverConnection={ this.serverConnection }
        resourceMap={this.resourceMap} />
    );
  }
}

function mapStateToProps(appState){
  return { appState };
}

function mapDispatchToProps(dispatch){
  return bindActionCreators(actionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NocheApp);
