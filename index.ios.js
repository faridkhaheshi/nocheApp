/**
 This is the project that will change the way people pay using internet.
 The famous Noche.
 */
import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import App from './app/containers/App';


export default class noche extends Component {

  constructor(props) {
    super(props);

    console.disableYellowBox = true;
  }

  render() {return <App />;}
}

AppRegistry.registerComponent('noche', () => noche);
